# Kafka Streams POJO support
###### Java 1.10+ Support
This project is now based on Kafka 2.0.x and has **dropped support for Java 7.**

When working with Kafka Streams the code can get very fragmented since all the stream operators have their own interface which needs to be implemented.

Using lambda expressions in the StreamBuilder is not an option since these are not testable (unit test) or debug-able (since the breakpoint only stops when the Topology is built not when events are processed).

Suppose we want 4 predicates, 3 foreach actions, 2 Select Key, 1 KeyValueMapper and ValueJoiner in a certain workflow regarding a single business entity.
, you would end up with 11 different classes instead of one Business Logic class.

This project enables POJOs to be used by streaming operators by providing builders that can build implementations of theses from annotated methods.

```java
CustomerServce customerService = ..

KStream<String, Customer> customerStream = kStreamBuilder.stream("customer-topic");

KStream<String, Customer>[] branchedStreams = customerStream.branch(new PredicateBuilder(customerService).methodName("hasUnpaidBills").build(),
                                                                    new PredicateBuilder(customerService).methodName("others").build());

branchedStreams[0].foreach(new ForeachNuilder(customerService).methodName("notifySales").build())

branchedStreams[1].to("approved_customer_topic");

```

To achieve an even more fluent solution than builders, a specialized KStream amd KStreamBuilder that uses method names and annotations can be used called PojoKstream and PojoKStreamBuilders. 

```java
CustomerServce customerService = ..
PojoKStreamBuilder pojoKStreamBuilder = new PojoKStreamBuilder(kStreamBuilder);

PojoKStream<String, Customer> customerStream = pojoKStreamBuilder.stream("customer-topic");

Map<String, PojoKStream<String, Customer>> branchedStreams = customerStream.branch(customerService, "hasUnpaidBills", "others");

branchedStreams.get("hasUnpaidBills").foreach(customerService, "notifySales");

branchedStreams.get("others").to("approved_customer_topic");

```

## Builders
A Builder can bind an annotated method on a POJO to a Kafka Streams Operator like KeyValueMapper, Predicate or ForeachAction.
See table below for the full list of supported operators.

For operators that support two parameters the POJO method has to annotate its parameters with ```@Key```, ```@Value``` or ```@JoinedValue``` in order to bind the correct parameter value.

Builders can bind methods by name and or type of parameters and return type.

| Name                  | Annotation            | Operator Type   | Method Return Type        | Parameter Annotations        |
|-----------------------|-----------------------|-----------------|---------------------------|------------------------------|
| FlatMapBuilder        | ```@Map```            | KeyValueMapper  | Iterable<KeyValue<KR, VR> | ```@Key, @Value```           |
| FlatMapValuesBuilder  | ```@FlatMap```        | ValueMapper     | Iterable<VR>              | -                            |
| ForeachBuilder        | ```@Foreach```        | ForeachAction   | void                      | ```@Key, @Value```           |
| KeyValueMapperBuilder | ```@KeyValueMapper``` | KeyValueMapper  | VR                        | ```@Key, @Value```           |
| MapBuilder            | ```@Map```            | KeyValueMapper  | KeyValue<KR, VR>          | ```@Key, @Value```           |
| PredicateBuilder      | ```@Predicate```      | Predicate       | boolean                   | ```@Key, @Value```           |
| SelectKeyBuilder      | ```@SelectKey```      | KeyValueMapper  | KR                        | ```@Key, @Value```           |
| ValueJoinerBuilder    | ```@ValueJoiner```    | ValueJoiner     | VR                        | ```@Value, @JoinedValue```   |
| ValueMapperBuilder    | ```@ValueMapper```    | ValueMapper     | VR                        | -                            |

##### Example:

```java
public class CustomerServiceImpl implements CustomerService {
    
    @Predicate
    public boolean hasUnpaidBills(@Value Customer customer) {
         ...
         return ...
    }
    
    @Predicate
    public boolean isBlocked(@Value Customer customer) {
         ...
         return ...
    }
    
    @Predicate
    public boolean notEuCustomer(@Value Customer customer) {
        ...
        return ...
    }
    
    @Predicate
    public boolean others() {
        return true;
    }
    
    @Foreach
    public void printCustomer(@Key customerId, @Value Customer) {
        ...
    }
    
    @Foreach
    public void notifyCustomer(@Value Customer) {
        ...
    }
    
    @Foreach
    public void notifySales(@Value Customer) {
        ...
    }
    
    @SelectKey
    public void selectEmailAsKey(@Value Customer customer) {
        return customer.getEmail();
    }
    
    @SelectKey
    public void selectCustomerIdAsKey(@Value Customer customer) {
        return customer.getId();
    }
    
    @KeyValueMapper
    public void mapKeyAsCountry(@Value Customer customer) {
        return customer.getAddress().getCountry();
    }
    
    @ValueJoiner
    public CustomerWithCountryStats joinCustomerAndCountry(@Value Customer, @JoinedValue CountryStats) {
        return ...
    }
}
```


