package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.kafka.streams.annotation.KeyValueMapper;
import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

@RunWith(MockitoJUnitRunner.class)
public class KeyValueMapperMethodTest {
    @Mock
    private MethodInvoker methodInvoker;

    @Mock
    private ParameterResolver parameterResolver;

    @Mock
    private Object returnValue;


    @Test
    public void run() throws NoSuchMethodException, IllegalAccessException {
        MyClass pojo = new MyClass();
        Method method = MyClass.class.getMethod("foreach", String.class, Integer.class);
        String key = "key";
        Integer value = 1;
        Object[] resolvedParameters =  new Object[]{key, value};
        KeyValueMapperMethod keyValueMapperMethod = new KeyValueMapperMethod(pojo, method, parameterResolver, KeyValueMapper.class);
        FieldUtils.writeDeclaredField(keyValueMapperMethod, "methodInvoker", methodInvoker, true);

        when(parameterResolver.resolveParameters(key, value)).thenReturn(resolvedParameters);
        when(methodInvoker.invoke(pojo, resolvedParameters)).thenReturn(returnValue);


        assertThat(keyValueMapperMethod.apply(key, value), equalTo(returnValue));


    }

    private static class MyClass {
        
        public String foreach(String key, Integer value) {
            return "ReturnValue";
        }

    }

}
