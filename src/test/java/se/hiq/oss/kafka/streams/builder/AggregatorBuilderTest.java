package se.hiq.oss.kafka.streams.builder;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.Aggregator;
import org.apache.kafka.streams.kstream.Initializer;
import org.junit.Test;

import se.hiq.oss.kafka.streams.annotation.AggregatedValue;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;

public class AggregatorBuilderTest {

    @Test
    public void buildByName() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);
        String aggregatedValue = "aggValue";

        AggregatorBuilder<String, Integer, String> aggregatorBuilder = new AggregatorBuilder(testPojo).methodName("aggregate");
        Aggregator<String, Integer, String> aggregator = aggregatorBuilder.build();
        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        Initializer<String> initializer = aggregatorBuilder.buildInitializer();
        assertThat(initializer.apply(), equalTo("initString"));

        aggregatorBuilder = new AggregatorBuilder(testPojo).methodName("aggregateReverseParams");
        aggregator = aggregatorBuilder.build();
        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));


        aggregatorBuilder = new AggregatorBuilder(testPojo).methodName("aggregateNoKey");
        aggregator = aggregatorBuilder.build();
        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo("key"));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        aggregatorBuilder = new AggregatorBuilder(testPojo).methodName("aggregateNoValue");
        aggregator = aggregatorBuilder.build();
        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(1));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        aggregatorBuilder = new AggregatorBuilder(testPojo).methodName("aggregateNoArgs");
        aggregator = aggregatorBuilder.build();
        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo("key"));
        assertThat(testPojo.getValue(), equalTo(1));
        assertThat(testPojo.getAggregatedValue(), equalTo("aggregatedValue"));

    }



    @Test
    public void buildByNameAndTypes() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);
        String aggregatedValue = "aggValue";

        AggregatorBuilder<String, Integer, String> aggregatorBuilder = new AggregatorBuilder(testPojo)
                .methodName("aggregate")
                .key(String.class)
                .value(Integer.class)
                .aggregateValue(String.class);
        Aggregator<String, Integer, String> aggregator = aggregatorBuilder.build();

        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        Initializer<String> initializer = aggregatorBuilder.buildInitializer();
        assertThat(initializer.apply(), equalTo("initString"));
    }

    @Test
    public void buildTypes() {
        TestPojo2 testPojo = new TestPojo2();
        String key = "key";
        Integer value = Integer.valueOf(10);
        String aggregatedValue = "aggValue";

        AggregatorBuilder<String, Integer, String> aggregatorBuilder = new AggregatorBuilder(testPojo)
                .key(String.class)
                .value(Integer.class)
                .aggregateValue(String.class);
        Aggregator<String, Integer, String> aggregator = aggregatorBuilder.build();

        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        Initializer<String> initializer = aggregatorBuilder.buildInitializer();
        assertThat(initializer.apply(), equalTo("initString"));
    }


    @Test
    public void buildDefault() {
        TestPojo2 testPojo = new TestPojo2();
        String key = "key";
        Integer value = Integer.valueOf(10);
        String aggregatedValue = "aggValue";

        AggregatorBuilder<String, Integer, String> aggregatorBuilder = new AggregatorBuilder(testPojo);
        Aggregator<String, Integer, String> aggregator = aggregatorBuilder.build();

        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        Initializer<String> initializer = aggregatorBuilder.buildInitializer();
        assertThat(initializer.apply(), equalTo("initString"));
    }


    @Test(expected = IllegalArgumentException.class)
    public void initializerWrongReturnType() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);
        String aggregatedValue = "aggValue";

        AggregatorBuilder<String, Integer, String> aggregatorBuilder = new AggregatorBuilder(testPojo)
                .methodName("aggregateInitWrongType")
                .key(String.class)
                .value(Integer.class)
                .aggregateValue(String.class);

        Aggregator<String, Integer, String> aggregator = aggregatorBuilder.build();

        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        aggregatorBuilder.buildInitializer();

    }

    @Test(expected = IllegalArgumentException.class)
    public void initializerNotTypedWrongReturnType() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);
        String aggregatedValue = "aggValue";

        AggregatorBuilder<String, Integer, String> aggregatorBuilder = new AggregatorBuilder(testPojo)
                .methodName("aggregateInitWrongType");
        Aggregator<String, Integer, String> aggregator = aggregatorBuilder.build();

        aggregator.apply(key, value, aggregatedValue);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));
        assertThat(testPojo.getAggregatedValue(), equalTo(aggregatedValue));

        aggregatorBuilder.buildInitializer();

    }


    public static class TestPojo {

        private String key;
        private Integer value;
        private String aggregatedValue;

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "init")
        public String aggregate(@Key String key, @Value Integer value, @AggregatedValue String aggregatedValue) {
            this.value = value;
            this.key = key;
            this.aggregatedValue = aggregatedValue;
            return aggregatedValue;
        }

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "init")
        public String aggregateReverseParams(@AggregatedValue String aggregatedValue, @Value Integer value, @Key String key) {
            this.value = value;
            this.key = key;
            this.aggregatedValue = aggregatedValue;
            return aggregatedValue;
        }

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "init")
        public String aggregateNoKey(@AggregatedValue String aggregatedValue, @Value Integer value) {
            this.value = value;
            this.key = "key";
            this.aggregatedValue = aggregatedValue;
            return aggregatedValue;
        }

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "init")
        public String aggregateNoValue(@AggregatedValue String aggregatedValue, @Key String key) {
            this.value = 1;
            this.key = key;
            this.aggregatedValue = aggregatedValue;
            return aggregatedValue;
        }

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "init")
        public String aggregateNoArgs() {
            this.value = 1;
            this.key = "key";
            this.aggregatedValue = "aggregatedValue";
            return aggregatedValue;
        }

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "intInit")
        public String aggregateInitWrongType(@Key String key, @Value Integer value, @AggregatedValue String aggregatedValue) {
            this.value = value;
            this.key = key;
            this.aggregatedValue = aggregatedValue;
            return aggregatedValue;
        }


        public String init() {
            return "initString";
        }

        public Integer intInit() {
            return 1;
        }

        public String getKey() {
            return key;
        }

        public Integer getValue() {
            return value;
        }

        public String getAggregatedValue() {
            return aggregatedValue;
        }

    }

    public static class TestPojo2 {

        private String key;
        private Integer value;
        private String aggregatedValue;

        @se.hiq.oss.kafka.streams.annotation.Aggregator(initializerMethod = "init")
        public String aggregate(@Value Integer value, @AggregatedValue String aggregatedValue, @Key String key) {
            this.value = value;
            this.key = key;
            this.aggregatedValue = aggregatedValue;
            return aggregatedValue;
        }

        public String init() {
            return "initString";
        }

        public String getKey() {
            return key;
        }

        public Integer getValue() {
            return value;
        }

        public String getAggregatedValue() {
            return aggregatedValue;
        }
    }
}
