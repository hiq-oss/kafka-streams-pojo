package se.hiq.oss.kafka.streams.builder;


import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.ValueMapper;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.Value;


public class ValueMapperBuilderTest {

    @Test
    public void buildByName() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Integer, String> valueMapper = new ValueMapperBuilder<Integer, String>(testPojo).methodName("valueMapperValue").build();

        valueMapper.apply(value);

        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new ValueMapperBuilder<Integer, String>(new TestPojo()).methodName("valueMapperSameName").build();
    }

    @Test
    public void buildByNameAndType() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Long, String> valueMapper = new ValueMapperBuilder<Long, String>(testPojo).methodName("valueMapperSameName").from(Long.class).to(String.class).build();

        valueMapper.apply(value.longValue());

        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameAndTypeMissing() {
       new ValueMapperBuilder<Double, String>(new TestPojo()).methodName("valueMapperSameName").from(Double.class).to(String.class).build();
    }

    @Test
    public void buildByNameAndTypes() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Integer, String> valueMapper = new ValueMapperBuilder<Integer, String>(testPojo).methodName("valueMapperValue").from(Integer.class).to(String.class).build();

        valueMapper.apply(value);

        assertThat(testPojo.getValue(), equalTo(value));
    }


    @Test
    public void buildByTypes() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Integer, String> valueMapper = new ValueMapperBuilder<Integer, String>(testPojo).from(Integer.class).to(String.class).build();

        valueMapper.apply(value);

        assertThat(testPojo.getValue(), equalTo(value));
    }



    @Test(expected = IllegalArgumentException.class)
    public void buildDefaultDuplicateMethods() {
        new ValueMapperBuilder(new TestPojo()).build();
    }

    public static class TestPojo {

        private Integer value;

        @se.hiq.oss.kafka.streams.annotation.ValueMapper
        public String valueMapperValue(@Value Integer value) {
            this.value = value;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.ValueMapper
        public String valueMapperSameName(@Value Long value) {
            this.value = value.intValue();
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.ValueMapper
        public String valueMapperSameName(@Value List<Long> value) {
            this.value = value.get(0).intValue();
            return "";
        }


        public Integer getValue() {
            return value;
        }
    }
}
