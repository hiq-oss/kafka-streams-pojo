package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

@RunWith(MockitoJUnitRunner.class)
public class PredicateMethodTest {
    @Mock
    private MethodInvoker methodInvoker;

    @Mock
    private ParameterResolver parameterResolver;

    private boolean returnValue = false;


    @Test
    public void run() throws NoSuchMethodException, IllegalAccessException {
        MyClass pojo = new MyClass();
        Method method = MyClass.class.getMethod("foreach", String.class, Integer.class);
        String key = "key";
        Integer value = 1;
        Object[] resolvedParameters =  new Object[]{key, value};
        PredicateMethod predicateMethod = new PredicateMethod(pojo, method, parameterResolver);
        FieldUtils.writeDeclaredField(predicateMethod, "methodInvoker", methodInvoker, true);

        when(parameterResolver.resolveParameters(key, value)).thenReturn(resolvedParameters);
        when(methodInvoker.invoke(pojo, resolvedParameters)).thenReturn(returnValue);


        assertThat(predicateMethod.test(key, value), is(returnValue));
        

    }

    private static class MyClass {
        
        public String foreach(String key, Integer value) {
            return "ReturnValue";
        }

    }

}
