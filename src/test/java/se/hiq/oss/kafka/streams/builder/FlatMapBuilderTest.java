package se.hiq.oss.kafka.streams.builder;


import java.util.ArrayList;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.FlatMap;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;

public class FlatMapBuilderTest {

    @Test
    public void buildByName() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new FlatMapBuilder(pojo).methodName("flatMapReverse").build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value));
    }

    @Test
    public void buildByNameNoArgs() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new FlatMapBuilder(pojo).methodName("flatMapNoArgs").build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo("noArgs"));
        assertThat(pojo.getValue(), nullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingAnnotation() {
        new FlatMapBuilder(new TestPojo()).methodName("flatMapMissingAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameExtraParameter() {
        new FlatMapBuilder(new TestPojo()).methodName("flatMapExtraParam").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingKeyAnnotation() {
        new FlatMapBuilder(new TestPojo()).methodName("flatMapMissingKeyAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingValueAnnotation() {
        new FlatMapBuilder(new TestPojo()).methodName("flatMapMissingValueAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new FlatMapBuilder(new TestPojo()).methodName("flatMap").build();
    }

    @Test
    public void buildByTypes() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Long value = Long.valueOf(10);

        KeyValueMapper<String, Long, String> keyValueMapper = new FlatMapBuilder(pojo).key(String.class).value(Long.class).build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value.intValue()));

    }


    @Test
    public void buildDefault() {
        TestPojo2 pojo = new TestPojo2();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new FlatMapBuilder(pojo).build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalStateException.class)
    public void wrongArgType() {
        TestPojo2 pojo = new TestPojo2();
        String key = "key";
        String value = "10";

        KeyValueMapper keyValueMapper = new FlatMapBuilder(pojo).build();
        keyValueMapper.apply(key, value);

    }

    @Test(expected = IllegalArgumentException.class)
    public void buildDefaultTooManyMethods() {
        TestPojo pojo = new TestPojo();
        new FlatMapBuilder(pojo).build();
    }


    public static class TestPojo {

        private Integer value;
        private String key;

        @FlatMap
        public Iterable<KeyValue<String, Integer>> flatMap(@Key String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMapExtraParam(@Key String key, @Value Integer value, String extraParam) {
            this.value = value;
            this.key = key;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMap(@Key String key, @Value Long value) {
            this.value = value.intValue();
            this.key = key;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMap(@Key String key) {
            this.key = key;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMap(@Value Integer value) {
            this.value = value;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMapMissingAnnotation(Integer value) {
            this.value = value;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMapNoArgs() {
            this.key = "noArgs";
            return new ArrayList<>();
        }



        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMapReverse(@Value Integer value, @Key String key) {
            this.value = value;
            this.key = key;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMapMissingKeyAnnotation(String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return new ArrayList<>();
        }

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMapMissingValueAnnotation(@Key String key, Integer value) {
            this.value = value;
            this.key = key;
            return new ArrayList<>();
        }




        public Integer getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }

    public static class TestPojo2 {
        private Integer value;
        private String key;

        @FlatMap
        public Iterable<KeyValue<String, Integer>>  flatMap(@Key String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return new ArrayList<>();
        }

        public Integer getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }
}
