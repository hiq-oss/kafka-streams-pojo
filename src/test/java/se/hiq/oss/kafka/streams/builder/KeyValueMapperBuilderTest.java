package se.hiq.oss.kafka.streams.builder;


import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;

public class KeyValueMapperBuilderTest {

    @Test
    public void buildByName() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new KeyValueMapperBuilder(pojo).methodName("keyValueMapperReverse").build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value));
    }

    @Test
    public void buildByNameNoArgs() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new KeyValueMapperBuilder(pojo).methodName("keyValueMapperNoArgs").build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo("noArgs"));
        assertThat(pojo.getValue(), nullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingAnnotation() {
        new KeyValueMapperBuilder(new TestPojo()).methodName("keyValueMapperMissingAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameExtraParameter() {
        new KeyValueMapperBuilder(new TestPojo()).methodName("keyValueMapperExtraParam").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingKeyAnnotation() {
        new KeyValueMapperBuilder(new TestPojo()).methodName("keyValueMapperMissingKeyAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingValueAnnotation() {
        new KeyValueMapperBuilder(new TestPojo()).methodName("keyValueMapperMissingValueAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new KeyValueMapperBuilder(new TestPojo()).methodName("keyValueMapper").build();
    }


    @Test
    public void buildByTypes() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Long value = Long.valueOf(10);

        KeyValueMapper<String, Long, String> keyValueMapper = new KeyValueMapperBuilder(pojo).key(String.class).value(Long.class).to(String.class).build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value.intValue()));

    }


    @Test
    public void buildDefault() {
        TestPojo2 pojo = new TestPojo2();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new KeyValueMapperBuilder(pojo).build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalStateException.class)
    public void wrongArgType() {
        TestPojo2 pojo = new TestPojo2();
        String key = "key";
        String value = "10";

        KeyValueMapper keyValueMapper = new KeyValueMapperBuilder(pojo).build();
        keyValueMapper.apply(key, value);

    }

    @Test(expected = IllegalArgumentException.class)
    public void buildDefaultTooManyMethods() {
        new KeyValueMapperBuilder(new TestPojo()).build();
    }


    public static class TestPojo {

        private Integer value;
        private String key;

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapper(@Key String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapperExtraParam(@Key String key, @Value Integer value, String extraParam) {
            this.value = value;
            this.key = key;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapper(@Key String key, @Value Long value) {
            this.value = value.intValue();
            this.key = key;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapper(@Key String key) {
            this.key = key;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapper(@Value Integer value) {
            this.value = value;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapperMissingAnnotation(Integer value) {
            this.value = value;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapperNoArgs() {
            this.key = "noArgs";
            return "";
        }



        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapperReverse(@Value Integer value, @Key String key) {
            this.value = value;
            this.key = key;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapperMissingKeyAnnotation(String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapperMissingValueAnnotation(@Key String key, Integer value) {
            this.value = value;
            this.key = key;
            return "";
        }




        public Integer getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }

    public static class TestPojo2 {
        private Integer value;
        private String key;

        @se.hiq.oss.kafka.streams.annotation.KeyValueMapper
        public String keyValueMapper(@Key String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return "";
        }

        public Integer getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }
}
