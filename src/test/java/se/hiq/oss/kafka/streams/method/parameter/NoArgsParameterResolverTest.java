package se.hiq.oss.kafka.streams.method.parameter;


import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class NoArgsParameterResolverTest {

    private NoArgsParameterResolver resolver = new NoArgsParameterResolver();

    @Test
    public void test() {
        Object[] args = {"One", "Two", "Three"};
        
        assertArrayEquals(new Object[]{}, resolver.resolveParameters(args));
    }
}
