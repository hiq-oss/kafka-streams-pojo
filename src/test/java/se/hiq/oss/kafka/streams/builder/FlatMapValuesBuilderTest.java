package se.hiq.oss.kafka.streams.builder;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.ValueMapper;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.FlatMapValues;
import se.hiq.oss.kafka.streams.annotation.Value;


public class FlatMapValuesBuilderTest {

    @Test
    public void buildByName() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Integer, Iterable<String>> valueMapper = new FlatMapValuesBuilder<Integer, String>(testPojo).methodName("flatMapValues").build();

        valueMapper.apply(value);

        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new FlatMapValuesBuilder<Integer, String>(new TestPojo()).methodName("flatMapValuesSameName").build();
    }

    @Test
    public void buildByNameAndType() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Long, Iterable<String>> valueMapper = new FlatMapValuesBuilder<Long, String>(testPojo).methodName("flatMapValuesSameName").from(Long.class).build();

        valueMapper.apply(value.longValue());

        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameAndTypeMissing() {
       new FlatMapValuesBuilder<Double, String>(new TestPojo()).methodName("flatMapValuesSameName").from(Double.class).build();
    }

    @Test
    public void buildByNameAndTypes() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Integer, Iterable<String>> valueMapper = new FlatMapValuesBuilder<Integer, String>(testPojo).methodName("flatMapValues").from(Integer.class).build();

        valueMapper.apply(value);

        assertThat(testPojo.getValue(), equalTo(value));
    }


    @Test
    public void buildByTypes() {
        TestPojo testPojo = new TestPojo();
        Integer value = Integer.valueOf(10);

        ValueMapper<Integer, Iterable<String>> valueMapper = new FlatMapValuesBuilder<Integer, String>(testPojo).from(Integer.class).build();

        valueMapper.apply(value);

        assertThat(testPojo.getValue(), equalTo(value));
    }



    @Test(expected = IllegalArgumentException.class)
    public void buildDefaultDuplicateMethods() {
        new FlatMapValuesBuilder(new TestPojo()).build();
    }

    public static class TestPojo {

        private Integer value;

        @FlatMapValues
        public Iterable<String> flatMapValues(@Value Integer value) {
            this.value = value;
            return new ArrayList<>();
        }

        @FlatMapValues
        public Iterable<String>  flatMapValuesSameName(@Value Long value) {
            this.value = value.intValue();
            return new ArrayList<>();
        }

        @FlatMapValues
        public Iterable<String>  flatMapValuesSameName(@Value List<Long> value) {
            this.value = value.get(0).intValue();
            return new ArrayList<>();
        }


        public Integer getValue() {
            return value;
        }
    }
}
