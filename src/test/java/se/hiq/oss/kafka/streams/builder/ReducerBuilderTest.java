package se.hiq.oss.kafka.streams.builder;


import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Reducer;
import org.junit.Test;

import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;

public class ReducerBuilderTest {

    @Test
    public void buildByName() {
        TestPojo testPojo = new TestPojo();
        String value1 = "value1";
        String value2 = "value2";

        Reducer reducer = new ReducerBuilder<>(testPojo).methodName("reduce").build();
        reducer.apply(value1, value2);
        assertThat(testPojo.getValue1(), equalTo(value1));
        assertThat(testPojo.getValue2(), equalTo(value2));

    }

    @Test
    public void buildByDefault() {
        TestPojo2 testPojo2 = new TestPojo2();
        String value1 = "value1";
        String value2 = "value2";

        Reducer reducer = new ReducerBuilder<>(testPojo2).build();
        reducer.apply(value1, value2);
        assertThat(testPojo2.getValue1(), equalTo(value1));
        assertThat(testPojo2.getValue2(), equalTo(value2));
    }

    @Test
    public void buildByType() {
        TestPojo testPojo = new TestPojo();
        String value1 = "value1";
        String value2 = "value2";

        Reducer<String> reducer = new ReducerBuilder<String>(testPojo).value(String.class).build();
        reducer.apply(value1, value2);
        assertThat(testPojo.getValue1(), equalTo(value1));
        assertThat(testPojo.getValue2(), equalTo(value2));

    }


    @Test
    public void buildByNameAndType() {
        TestPojo testPojo = new TestPojo();
        String value1 = "value1";
        String value2 = "value2";

        Reducer<String> reducer = new ReducerBuilder<String>(testPojo).methodName("reduce").value(String.class).build();
        reducer.apply(value1, value2);
        assertThat(testPojo.getValue1(), equalTo(value1));
        assertThat(testPojo.getValue2(), equalTo(value2));

    }


    @Test(expected = IllegalArgumentException.class)
    public void buildByNameOneParameter() {
        new ReducerBuilder(new TestPojo()).methodName("reduceOneParameter").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameParameterTypeNotMatching() {
        new ReducerBuilder(new TestPojo()).methodName("reduceDifferentArgTypes").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameReturnTypeNotMatching() {
        new ReducerBuilder(new TestPojo()).methodName("reduceDifferentReturnType").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameNoArgs() {
        new ReducerBuilder(new TestPojo()).methodName("reduceNoArgs").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameThreeArgs() {
        new ReducerBuilder(new TestPojo()).methodName("reduceThreeArgs").build();
    }



    public static class TestPojo {

        private String value1;
        private String value2;

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public String reduce(String value1, String value2) {
            this.value1 = value1;
            this.value2 = value2;
            return value2;
        }

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public String reduceThreeArgs(String value1, String value2, String value3) {
            this.value1 = value1;
            this.value2 = value2;
            return value2;
        }

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public String reduceNoArgs() {
            return "value2";
        }

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public String reduceOneParameter(String value1) {
            this.value1 = value1;
            return value2;
        }

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public String reduceDifferentArgTypes(String value1, Integer value2) {
            this.value1 = value1;
            this.value2 = value2.toString();
            return value1;
        }

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public Integer reduceDifferentReturnType(String value1, String value2) {
            this.value1 = value1;
            this.value2 = value2;
            return 1;
        }


    public String getValue1() {
        return value1;
    }

    public String getValue2() {
        return value2;
    }
}

    public static class TestPojo2 {

        private String value1;
        private String value2;

        @se.hiq.oss.kafka.streams.annotation.Reducer
        public String reduce(String value1, String value2) {
            this.value1 = value1;
            this.value2 = value2;
            return value1;
        }

        public String getValue1() {
            return value1;
        }

        public String getValue2() {
            return value2;
        }
    }
}
