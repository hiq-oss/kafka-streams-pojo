package se.hiq.oss.kafka.streams.method;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.kafka.streams.annotation.KeyValueMapper;

@RunWith(MockitoJUnitRunner.class)
public class MethodInvokerTest {


    @Test
    public void invokeSuccess() throws NoSuchMethodException {
        MyClass pojo = new MyClass();
        MethodInvoker methodInvoker = new MethodInvoker(MyClass.class.getMethod("theMethod"), KeyValueMapper.class);
        assertThat(methodInvoker.invoke(pojo), equalTo((Object)"ReturnValue"));

    }

    @Test(expected = IllegalStateException.class)
    public void invokeIllegalAccess() throws NoSuchMethodException {
        MyClass pojo = new MyClass();
        MethodInvoker methodInvoker = new MethodInvoker(MyClass.class.getDeclaredMethod("privateMethod"), KeyValueMapper.class);
        methodInvoker.invoke(pojo);
    }

    @Test(expected = IllegalStateException.class)
    public void invokeInvocationTargetException() throws NoSuchMethodException {
        MyClass pojo = new MyClass();
        MethodInvoker methodInvoker = new MethodInvoker(MyClass.class.getDeclaredMethod("notYetImplemented"), KeyValueMapper.class);
        methodInvoker.invoke(pojo);
    }

    @Test(expected = IllegalStateException.class)
    public void invokeIllegalArgument() throws NoSuchMethodException {
        MyClass pojo = new MyClass();
        MethodInvoker methodInvoker = new MethodInvoker(MyClass.class.getMethod("argMethod", String.class), KeyValueMapper.class);
        methodInvoker.invoke(pojo, 1);
    }



    private static class MyClass {
        public String theMethod() {
            return "ReturnValue";
        }

        private String privateMethod() {
            return "ReturnValue";
        }

        public String notYetImplemented() {
            throw new RuntimeException("Not yet implemented");
        }

        public String argMethod(String arg) {
            return "ReturnValue";
        }

    }
}
