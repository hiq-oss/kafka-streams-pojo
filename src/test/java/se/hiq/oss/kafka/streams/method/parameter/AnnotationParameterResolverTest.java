package se.hiq.oss.kafka.streams.method.parameter;


import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import se.hiq.oss.kafka.streams.annotation.JoinedValue;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;

public class AnnotationParameterResolverTest {


    @Test
    public void joindValues() throws NoSuchMethodException {

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyClass.class.getMethod("joinValues", String.class, Integer.class), Value.class, JoinedValue.class);

        assertArrayEquals(new Object[]{"value", 1},  resolver.resolveParameters("value", 1));
    }

    @Test
    public void joindValuesReverse() throws NoSuchMethodException {

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyClass.class.getMethod("joinValuesReverse", Integer.class, String.class), Value.class, JoinedValue.class);

        assertArrayEquals(new Object[]{1, "value"},  resolver.resolveParameters("value", 1));
    }

    @Test
    public void joinValue() throws NoSuchMethodException {

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyClass.class.getMethod("joinValue", String.class), Value.class, JoinedValue.class);

        assertArrayEquals(new Object[]{"value"},  resolver.resolveParameters("value", 1));
    }

    @Test
    public void joinValueJoined() throws NoSuchMethodException {

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyClass.class.getMethod("joinValue", Integer.class), Value.class, JoinedValue.class);

        assertArrayEquals(new Object[]{1},  resolver.resolveParameters("value", 1));
    }


    @Test
    public void joindValueNoArgs() throws NoSuchMethodException {

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyClass.class.getMethod("joinValue"), Value.class, JoinedValue.class);

        assertArrayEquals(new Object[]{},  resolver.resolveParameters("value", 1));
    }


    @Test(expected = IllegalArgumentException.class)
    public void missingOneAnnotation() throws NoSuchMethodException {

        new AnnotationParameterResolver(MyClass.class.getMethod("missingOneAnnotation", String.class, Integer.class), Value.class, JoinedValue.class);


    }

    @Test(expected = IllegalArgumentException.class)
    public void missingTwoAnnotations() throws NoSuchMethodException {

        new AnnotationParameterResolver(MyClass.class.getMethod("missingTwoAnnotation", String.class, Integer.class), Value.class, JoinedValue.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void missingAnnotation() throws NoSuchMethodException {

        new AnnotationParameterResolver(MyClass.class.getMethod("missingAnnotation", String.class), Value.class, JoinedValue.class);
    }




    @Test
    public void valueAnnotated() throws NoSuchMethodException {
        String key = "key";
        String value = "value";

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyOtherClass.class.getMethod("valueMethod", String.class), Key.class, Value.class);
        assertArrayEquals(new Object[]{value}, resolver.resolveParameters(key, value));

    }

    @Test
    public void keyAnnotated() throws NoSuchMethodException {
        String key = "key";
        String value = "value";

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyOtherClass.class.getMethod("keyMethod", String.class), Key.class, Value.class);
        assertArrayEquals(new Object[]{key}, resolver.resolveParameters(key, value));

    }

    @Test
    public void joinedValueAnnotated() throws NoSuchMethodException {
        String joinedValue = "joinedValue";
        String value = "value";

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyOtherClass.class.getMethod("joinedValueMethod", String.class), Value.class, JoinedValue.class);
        assertArrayEquals(new Object[]{joinedValue}, resolver.resolveParameters(value, joinedValue));

    }

    @Test(expected = IllegalArgumentException.class)
    public void missingAnnotation2() throws NoSuchMethodException {
        String key = "key";
        String value = "value";

        AnnotationParameterResolver resolver = new AnnotationParameterResolver(MyOtherClass.class.getMethod("missingAnnotation", String.class), Key.class, Value.class);
        assertArrayEquals(new Object[]{value}, resolver.resolveParameters(key, value));

    }


    private static class MyClass {
        public String joinValues(@Value String value, @JoinedValue Integer joindedValue) {
            return value + joindedValue;
        }

        public String joinValuesReverse(@JoinedValue Integer joindedValue, @Value String value) {
            return value + joindedValue;
        }

        public String joinValue(@Value String value) {
            return value;
        }

        public String joinValue(@JoinedValue Integer joinedValue) {
            return joinedValue.toString();
        }

        public String joinValue() {
            return "noArgs";
        }

        public String missingOneAnnotation(String value, @JoinedValue Integer joindedValue) {
            return value + joindedValue;
        }

        public String missingTwoAnnotation(String value, Integer joindedValue) {
            return value + joindedValue;
        }


        public String missingAnnotation(String value) {
            return value;
        }

    }

    private static class MyOtherClass {
        public void valueMethod(@Value String value) {

        }

        public void keyMethod(@Key String key) {

        }


        public void joinedValueMethod(@JoinedValue String leftValue) {

        }


        public void missingAnnotation(String value) {

        }
    }
}
