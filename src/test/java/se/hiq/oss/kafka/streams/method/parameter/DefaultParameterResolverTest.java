package se.hiq.oss.kafka.streams.method.parameter;


import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class DefaultParameterResolverTest {

    private DefaultParameterResolver resolver = new DefaultParameterResolver();

    @Test
    public void test() {
        Object[] args = {"One", "Two", "Three"};

        assertArrayEquals(args, resolver.resolveParameters(args));
    }
}
