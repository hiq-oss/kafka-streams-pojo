package se.hiq.oss.kafka.streams.builder;


import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Map;
import se.hiq.oss.kafka.streams.annotation.Value;

public class MapBuilderTest {

    @Test
    public void buildByName() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new MapBuilder(pojo).methodName("mapReverse").build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value));
    }

    @Test
    public void buildByNameNoArgs() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new MapBuilder(pojo).methodName("mapNoArgs").build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo("noArgs"));
        assertThat(pojo.getValue(), nullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingAnnotation() {
        new MapBuilder(new TestPojo()).methodName("mapMissingAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameExtraParameter() {
        new MapBuilder(new TestPojo()).methodName("mapExtraParam").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingKeyAnnotation() {
        new MapBuilder(new TestPojo()).methodName("mapMissingKeyAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameMissingValueAnnotation() {
        new MapBuilder(new TestPojo()).methodName("mapMissingValueAnnotation").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new MapBuilder(new TestPojo()).methodName("map").build();
    }


    @Test
    public void buildByTypes() {
        TestPojo pojo = new TestPojo();
        String key = "key";
        Long value = Long.valueOf(10);

        KeyValueMapper<String, Long, String> keyValueMapper = new MapBuilder(pojo).key(String.class).value(Long.class).build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value.intValue()));

    }


    @Test
    public void buildDefault() {
        TestPojo2 pojo = new TestPojo2();
        String key = "key";
        Integer value = Integer.valueOf(10);

        KeyValueMapper keyValueMapper = new MapBuilder(pojo).build();
        keyValueMapper.apply(key, value);

        assertThat(pojo.getKey(), equalTo(key));
        assertThat(pojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalStateException.class)
    public void wrongArgType() {
        TestPojo2 pojo = new TestPojo2();
        String key = "key";
        String value = "10";

        KeyValueMapper keyValueMapper = new MapBuilder(pojo).build();
        keyValueMapper.apply(key, value);

    }

    @Test(expected = IllegalArgumentException.class)
    public void buildDefaultTooManyMethods() {
      new MapBuilder(new TestPojo()).build();
    }


    public static class TestPojo {

        private Integer value;
        private String key;

        @Map
        public KeyValue<String, Integer> map(@Key String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> mapExtraParam(@Key String key, @Value Integer value, String extraParam) {
            this.value = value;
            this.key = key;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Long> map(@Key String key, @Value Long value) {
            this.value = value.intValue();
            this.key = key;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> map(@Key String key) {
            this.key = key;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> map(@Value Integer value) {
            this.value = value;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> mapMissingAnnotation(Integer value) {
            this.value = value;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> mapNoArgs() {
            this.key = "noArgs";
            return new KeyValue<>(key, value);
        }



        @Map
        public KeyValue<String, Integer> mapReverse(@Value Integer value, @Key String key) {
            this.value = value;
            this.key = key;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> mapMissingKeyAnnotation(String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return new KeyValue<>(key, value);
        }

        @Map
        public KeyValue<String, Integer> mapMissingValueAnnotation(@Key String key, Integer value) {
            this.value = value;
            this.key = key;
            return new KeyValue<>(key, value);
        }




        public Integer getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }

    public static class TestPojo2 {
        private Integer value;
        private String key;

        @Map
        public KeyValue<String, Integer> map(@Key String key, @Value Integer value) {
            this.value = value;
            this.key = key;
            return new KeyValue<>(key, value);
        }

        public Integer getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }
}
