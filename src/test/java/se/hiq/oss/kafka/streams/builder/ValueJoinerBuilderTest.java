package se.hiq.oss.kafka.streams.builder;


import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.ValueJoiner;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.JoinedValue;
import se.hiq.oss.kafka.streams.annotation.Value;


public class ValueJoinerBuilderTest {

    @Test
    public void buildByName() {
        TestPojo pojo = new TestPojo();
        String value = "value";
        Integer joinedValue = Integer.valueOf(10);

        ValueJoiner ValueJoiner = new ValueJoinerBuilder(pojo).methodName("valueJoiner").build();
        ValueJoiner.apply(value, joinedValue);

        assertThat(pojo.getValue(), equalTo(value));
        assertThat(pojo.getJoinedValue(), equalTo(joinedValue));

        pojo = new TestPojo();
        ValueJoiner = new ValueJoinerBuilder(pojo).methodName("valueJoinerReverse").build();
        ValueJoiner.apply(value, joinedValue);
    }

    @Test
    public void buildByTypes() {
        TestPojo pojo = new TestPojo();
        String value = "value";
        Long joinedValue = Long.valueOf(10);

        ValueJoiner<String, Long, String> ValueJoiner = new ValueJoinerBuilder(pojo).value(String.class).joinedValue(Long.class).to(String.class).build();
        ValueJoiner.apply(value, joinedValue);

        assertThat(pojo.getValue(), equalTo(value));
        assertThat(pojo.getJoinedValue(), equalTo(joinedValue.intValue()));

    }

    @Test(expected = IllegalArgumentException.class)
    public void buildDefaultMuötipleMethods() {
        new ValueJoinerBuilder(new TestPojo()).build();
    }




    public static class TestPojo {

        private Integer joinedValue;
        private String value;

        @se.hiq.oss.kafka.streams.annotation.ValueJoiner
        public String valueJoiner(@Value String value, @JoinedValue Integer joinedValue) {
            this.value = value;
            this.joinedValue = joinedValue;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.ValueJoiner
        public String valueJoinerReverse(@JoinedValue Integer joinedValue, @Value String value) {
            this.joinedValue = joinedValue;
            this.value = value;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.ValueJoiner
        public String valueJoinerSameName(@Value String value, @JoinedValue Long joinedValue) {
            this.joinedValue = joinedValue.intValue();
            this.value = value;
            return "";
        }

        @se.hiq.oss.kafka.streams.annotation.ValueJoiner
        public String valueJoinerSameName(@Value String value, @JoinedValue List<Long> joinedValue) {
            this.joinedValue = joinedValue.get(0).intValue();
            this.value = value;
            return "";
        }


        public Integer getJoinedValue() {
            return joinedValue;
        }

        public String getValue() {
            return value;
        }
    }
}
