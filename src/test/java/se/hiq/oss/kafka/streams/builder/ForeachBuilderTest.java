package se.hiq.oss.kafka.streams.builder;


import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.ForeachAction;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.Foreach;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;


public class ForeachBuilderTest {

    @Test
    public void buildByName() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        ForeachAction action = new ForeachBuilder(testPojo).methodName("forEachKeyValue").build();
        action.apply(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));

        testPojo = new TestPojo();
        action = new ForeachBuilder(testPojo).methodName("forEachKeyValueReverse").build();
        action.apply(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));

        testPojo = new TestPojo();
        action = new ForeachBuilder(testPojo).methodName("forEachKey").build();
        action.apply(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), nullValue());

        testPojo = new TestPojo();
        action = new ForeachBuilder(testPojo).methodName("forEachValue").build();
        action.apply(key, value);
        assertThat(testPojo.getKey(), nullValue());
        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new ForeachBuilder(new TestPojo()).methodName("forEachSameName").build();
    }

    @Test
    public void buildByNameAndType() {
        TestPojo testPojo = new TestPojo();
        Long key = 1L;

        ForeachAction<Long, Integer> action = new ForeachBuilder<Long, Integer>(testPojo).methodName("forEachSameName").key(Long.class).build();
        action.apply(key, 10);
        assertThat(testPojo.getKey(), equalTo(key.toString()));
        assertThat(testPojo.getValue(), nullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByInvalidName() {
        new ForeachBuilder(new TestPojo()).methodName("invalidMethodName").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByTypeMultiple() {
        new ForeachBuilder(new TestPojo()).key(String.class).value(Integer.class).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByTypeMissing() {
        new ForeachBuilder(new TestPojo()).key(String.class).value(List.class).build();
    }

    @Test
    public void buildByKeyType() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Short value = new Short("10");

        ForeachAction action = new ForeachBuilder(testPojo).key(String.class).build();
        action.apply(key, value);

        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), nullValue());
    }
    @Test
    public void buildByValueType() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Short value = new Short("10");

        ForeachAction action = new ForeachBuilder(testPojo).value(Short.class).build();
        action.apply(key, value);
        assertThat(testPojo.getKey(), nullValue());
        assertThat(testPojo.getValue(), equalTo(value.intValue()));
    }



    @Test
    public void buildByNameNoArgs() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        ForeachAction action = new ForeachBuilder(testPojo).methodName("forEachNoArgs").build();
        action.apply(key, value);
        assertThat(testPojo.getKey(), equalTo("noArgs"));
        assertThat(testPojo.getValue(), nullValue());
    }

    public static class TestPojo {

        private String key;
        private Integer value;

        @Foreach
        public void forEachKeyValue(@Key String key, @Value Integer value) {
            this.key = key;
            this.value = value;
        }

        @Foreach
        public void forEachKeyValueReverse(@Value Integer value, @Key String key) {
            this.key = key;
            this.value = value;
        }

        @Foreach
        public void forEachKey(@Key String key) {
            this.key = key;
        }

        @Foreach
        public void forEachValue(@Value Integer value) {
            this.value = value;
        }

        @Foreach
        public void forEachSameName(@Key Long key) {
            this.key = key.toString();
        }

        @Foreach
        public void forEachSameName(@Key Double key) {
            this.key = key.toString();
        }

        @Foreach
        public void forEachValueUnique(@Value Short value) {
            this.value = value.intValue();
        }

        @Foreach
        public void forEachNoArgs() {
            this.key = "noArgs";
        }


        public String getKey() {
            return key;
        }

        public Integer getValue() {
            return value;
        }
    }
}
