package se.hiq.oss.kafka.streams.builder;


import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.apache.kafka.streams.kstream.Predicate;
import org.junit.Test;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;

public class PredicateBuilderTest {

    @Test
    public void buildByName() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        Predicate predicate = new PredicateBuilder(testPojo).methodName("predicateKeyValue").build();
        predicate.test(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));

        testPojo = new TestPojo();
        predicate = new PredicateBuilder(testPojo).methodName("predicateKeyValueReverse").build();
        predicate.test(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), equalTo(value));

        testPojo = new TestPojo();
        predicate = new PredicateBuilder(testPojo).methodName("predicateKey").build();
        predicate.test(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), nullValue());

        testPojo = new TestPojo();
        predicate = new PredicateBuilder(testPojo).methodName("predicateValue").build();
        predicate.test(key, value);
        assertThat(testPojo.getKey(), nullValue());
        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByNameCollision() {
        new PredicateBuilder(new TestPojo()).methodName("predicateSameName").build();
    }

    @Test
    public void buildByNameAndTypes() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        Predicate predicate = new PredicateBuilder(testPojo).methodName("predicateSameName").value(Long.class).build();

        predicate.test(key, value.longValue());

        assertThat(testPojo.getKey(), nullValue());
        assertThat(testPojo.getValue(), equalTo(value));
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByInvalidName() {
        new PredicateBuilder(new TestPojo()).methodName("invalidMethodName").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByTypeMultiple() {
        new PredicateBuilder(new TestPojo()).key(String.class).value(Integer.class).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildByTypeMissing() {
        new PredicateBuilder(new TestPojo()).key(String.class).value(List.class).build();
    }

    @Test
    public void buildByKeyType() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        Predicate predicate = new PredicateBuilder(testPojo).key(String.class).build();
        predicate.test(key, value);
        assertThat(testPojo.getKey(), equalTo(key));
        assertThat(testPojo.getValue(), nullValue());
    }

    @Test
    public void buildByValueType() {
        TestPojo testPojo = new TestPojo();
        String key = "key";
        Integer value = Integer.valueOf(10);

        Predicate predicate = new PredicateBuilder(testPojo).value(Integer.class).build();
        predicate.test(key, value);
        assertThat(testPojo.getKey(), nullValue());
        assertThat(testPojo.getValue(), equalTo(value));
    }


    @Test
    public void buildDefault() {
        TestPojo2 testPojo = new TestPojo2();
        String key = "key";
        Integer value = Integer.valueOf(10);

        Predicate predicate = new PredicateBuilder(testPojo).build();

        predicate.test(key, value);
        assertThat(testPojo.getKey(), nullValue());
        assertThat(testPojo.getValue(), equalTo(value));
    }

    public static class TestPojo {

        private String key;
        private Integer value;

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateKeyValue(@Key String key, @Value Integer value) {
            this.key = key;
            this.value = value;
            return true;
        }

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateKeyValueReverse(@Value Integer value, @Key String key) {
            this.key = key;
            this.value = value;
            return true;
        }

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateKey(@Key String key) {
            this.key = key;
            return true;
        }

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateValue(@Value Integer value) {
            this.value = value;
            return true;
        }

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateSameName(@Value Double value) {
            this.value = value.intValue();
            return true;
        }

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateSameName(@Value Long value) {
            this.value = value.intValue();
            return true;
        }

        public String getKey() {
            return key;
        }

        public Integer getValue() {
            return value;
        }
    }

    public static class TestPojo2 {

        private String key;
        private Integer value;

        @se.hiq.oss.kafka.streams.annotation.Predicate
        public boolean predicateKeyValue(@Value Integer value) {
            this.value = value;
            return true;
        }

        public String getKey() {
            return key;
        }

        public Integer getValue() {
            return value;
        }
    }
}
