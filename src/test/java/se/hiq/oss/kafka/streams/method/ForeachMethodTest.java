package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import static org.mockito.Mockito.when;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

@RunWith(MockitoJUnitRunner.class)
public class ForeachMethodTest {
    @Mock
    private MethodInvoker methodInvoker;

    @Mock
    private ParameterResolver parameterResolver;


    @Test
    public void run() throws NoSuchMethodException, IllegalAccessException {
        MyClass pojo = new MyClass();
        Method method = MyClass.class.getMethod("foreach", String.class, Integer.class);
        String key = "key";
        Integer value = 1;
        Object[] resolvedParameters =  new Object[]{key, value};
        ForeachMethod foreachMethod = new ForeachMethod(pojo, method, parameterResolver);
        FieldUtils.writeDeclaredField(foreachMethod, "methodInvoker", methodInvoker, true);

        when(parameterResolver.resolveParameters(key, value)).thenReturn(resolvedParameters);
        when(methodInvoker.invoke(pojo, resolvedParameters)).thenReturn(null);


        foreachMethod.apply(key, value);

    }

    private static class MyClass {

        public void foreach(String key, Integer value) {
        }

    }

}
