package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.ForeachAction;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.annotation.Foreach;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;
import se.hiq.oss.kafka.streams.method.ForeachMethod;
import se.hiq.oss.kafka.streams.method.parameter.AnnotationParameterResolver;


/**
 * Builds a ForeachAction by resolving a single @Foreach annotated method from a POJO.
 *
 * Methods can be resolved by:
 * <ul>
 *     <li>By method name (methodName)</li>
 *     <li>By types (key and value)</li>
 *     <li>By method name and types (methodName, key, value)</li>
 * </ul>
 *
 * Note: Only methods with void return type and one or two arguments are considered.
 *
 * @param <K> Key Class
 * @param <V> Value Class
 */
@SuppressWarnings("checkstyle:hiddenfield")
public class ForeachBuilder<K, V> {

    private Object pojo;
    private String methodName;
    private Class<K> keyClass;
    private Class<V> valueClass;
    private MethodUtils methodUtils = new MethodUtils();

    public ForeachBuilder(@NotNull final Object pojo) {
        this.pojo = pojo;
    }

    /**
     * Builds a ForeachAction by resolving a single annotated method.
     *
     * @return ForeachAction for the method found
     *
     * @throws IllegalStateException    If methodName or keyClass or valueClass is not provided.
     * @throws IllegalArgumentException If a single method could not be resolved.
     **/
    public ForeachAction<K, V> build() {

        Method method = getSingleMethod();

        return new ForeachMethod<>(pojo,
                method,
                new AnnotationParameterResolver(method, Key.class, Value.class));
    }

    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(Foreach.class)
                .returnType(Void.TYPE);


        methodUtils.filterOnNameAndParameters(methodName, methodFilterBuilder, 2, keyClass, valueClass);

        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);

        methodUtils.validateSingleMethod(pojo, methods, methodFilter);
        return methods.iterator().next();

    }



    public ForeachBuilder<K, V> methodName(String methodName) {
       this.methodName = methodName;
        return this;
    }

    public ForeachBuilder<K, V> key(Class<K> keyClass) {
        this.keyClass = keyClass;
        return this;
    }

    public ForeachBuilder<K, V> value(Class<V> valueClass) {
       this.valueClass = valueClass;
        return this;
    }
}
