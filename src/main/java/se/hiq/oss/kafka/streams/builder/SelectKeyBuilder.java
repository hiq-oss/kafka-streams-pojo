package se.hiq.oss.kafka.streams.builder;


import javax.validation.constraints.NotNull;

import se.hiq.oss.kafka.streams.annotation.SelectKey;

public class SelectKeyBuilder<K, V, KR> extends KeyValueMapperBuilder<K, V, KR> {

    public SelectKeyBuilder(@NotNull final Object pojo) {
        super(pojo, SelectKey.class);
    }

}
