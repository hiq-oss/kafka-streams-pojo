package se.hiq.oss.kafka.streams;

import java.lang.reflect.Field;
import java.util.Set;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.kafka.streams.kstream.internals.AbstractStream;
import org.apache.kafka.streams.kstream.internals.InternalStreamsBuilder;
import org.apache.kafka.streams.kstream.internals.KStreamImpl;

@SuppressFBWarnings("DP_DO_INSIDE_DO_PRIVILEGED")
final class KStreamUtils {

    private static Field sourceNodeField;
    private static Field repartitionRequiredField;
    private static Field nameField;
    private static Field builderField;

    private KStreamUtils() {
    }

    static {
        try {
            sourceNodeField = AbstractStream.class.getDeclaredField("sourceNodes");
            sourceNodeField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Could not find sourceNodes field", e);
        }
        try {
            repartitionRequiredField = KStreamImpl.class.getDeclaredField("repartitionRequired");
            repartitionRequiredField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Could not find repartitionRequired field", e);
        }

        try {
            nameField = AbstractStream.class.getDeclaredField("name");
            nameField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Could not find name field", e);
        }

        try {
            builderField = AbstractStream.class.getDeclaredField("builder");
            builderField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Could not find builder field", e);
        }
    }

    public static Set<String> getSourceNodes(KStreamImpl kStream) {
        try {
            return (Set<String>) sourceNodeField.get(kStream);
        } catch (IllegalAccessException e) {
            // Should not happen since accessible is set to true
            throw new IllegalStateException("Could not access field sourceNode.", e);
        }
    }

    public static boolean isRepartitionRequired(KStreamImpl kStream) {
        try {
            return (Boolean) repartitionRequiredField.get(kStream);
        } catch (IllegalAccessException e) {
            // Should not happen since accessible is set to true
            throw new IllegalStateException("Could not access field repartitionRequired.", e);
        }
    }

    public static InternalStreamsBuilder getBuilder(KStreamImpl kStream) {
        try {
            return (InternalStreamsBuilder) builderField.get(kStream);
        } catch (IllegalAccessException e) {
            // Should not happen since accessible is set to true
            throw new IllegalStateException("Could not access field builder.", e);
        }
    }

    public static String getName(KStreamImpl kStream) {
        try {
            return (String) nameField.get(kStream);
        } catch (IllegalAccessException e) {
            // Should not happen since accessible is set to true
            throw new IllegalStateException("Could not access field name.", e);
        }
    }

}
