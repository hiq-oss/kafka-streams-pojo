package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.ClassFilterBuilder;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;


public class MethodUtils {

    public void validateSingleMethod(Object pojo, Set<Method> methods, MethodFilter methodFilter) {
        if (methods.isEmpty()) {
            throw new IllegalArgumentException("No method found matching: " + methodFilter.describe());
        } else if (methods.size() > 1) {
            List<String> methodNames = new ArrayList<>();
            for (Method method : methods) {
                methodNames.add(method.toString().replace(pojo.getClass().getName() + ".", ""));
            }
            throw new IllegalArgumentException("More than one methods found matching: " + methodFilter.describe() + ". Found "
                    + methods.size() + " candidate methods for " + pojo.getClass() + ": " + methodNames
                    + " use methodName to qualify one of these.");
        }
    }



    public void filterOnNameAndParameters(String methodName,
                                          MethodFilterBuilder methodFilterBuilder,
                                          int maxNumberOfParameters,
                                          Class<?>... parameterTypes) {
        if (!StringUtils.isEmpty(methodName)) {
            methodFilterBuilder.name(methodName);
        }

        numberParametersFilter(methodFilterBuilder, maxNumberOfParameters, parameterTypes);

        Stream.of(parameterTypes).forEach(t ->  anyParameterOfType(methodFilterBuilder, t, maxNumberOfParameters));

    }

    private void anyParameterOfType(MethodFilterBuilder methodFilterBuilder,
                                    Class<?> parameterType,
                                    int maxNumberOfParameters) {
        if (parameterType != null) {
            for (int i = 0; i <  maxNumberOfParameters; i++) {
                methodFilterBuilder
                        .parameterTypeFilter(i, new ClassFilterBuilder().extendsType(parameterType).build());
                if (i < (maxNumberOfParameters - 1)) {
                    methodFilterBuilder.or();
                }
            }
        }
    }


    private void numberParametersFilter(MethodFilterBuilder methodFilterBuilder,
                                          int maxNumberOfParameters,
                                          Class<?> ... parameterTypes) {
        int nonNullCount = countNonNullElements(parameterTypes);

        if (nonNullCount > 0) {
            methodFilterBuilder.numberOfParameters(nonNullCount);
        } else {
            for (int i = 0; i <= maxNumberOfParameters; i++) {
                methodFilterBuilder.numberOfParameters(i);

                if (i < (maxNumberOfParameters)) {
                    methodFilterBuilder.or();
                }
            }
        }

    }

    @SuppressWarnings("PMD.UseVarargs")
    private int countNonNullElements(Object[] array) {
       return (int) Stream.of(array).filter(Objects::nonNull).count();
    }

}
