package se.hiq.oss.kafka.streams.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks an one or two parameter method with return type void as an For Each method.
 *
 * Parameters needs to be annotated with @Key and / or @Value
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Foreach {
}
