package se.hiq.oss.kafka.streams.method.parameter;


public interface ParameterResolver {

    Object[] resolveParameters(Object... parameters);
}
