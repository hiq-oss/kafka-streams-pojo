package se.hiq.oss.kafka.streams;

import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;


public interface PojoKGroupedStream<K, V> extends KGroupedStream<K, V> {


    KTable<K, V> reduce(final Object pojo,
                        final String reducerMethodName,
                        final Class<V> valueType);

    KTable<K, V> reduce(final Object pojo,
                        final String reducerMethodName,
                        final Class<V> valueType,
                        final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized);



    <VR> KTable<K, VR> aggregate(final Object pojo,
                                 final String aggregatorMethodName,
                                 final Class<K> keyType,
                                 final Class<V> valueType,
                                 final Class<VR> aggregatedValueType,
                                 final Materialized<K, VR, KeyValueStore<Bytes, byte[]>> materialized);

    <VR> KTable<K, VR> aggregate(final Object pojo,
                                 final String aggregatorMethodName,
                                 final Class<K> keyType,
                                 final Class<V> valueType,
                                 final Class<VR> aggregatedValueType);


}
