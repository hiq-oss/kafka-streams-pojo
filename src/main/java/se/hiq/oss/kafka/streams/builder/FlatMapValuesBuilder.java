package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.kstream.ValueMapper;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.annotation.FlatMapValues;
import se.hiq.oss.kafka.streams.method.ValueMapperMethod;
import se.hiq.oss.kafka.streams.method.parameter.DefaultParameterResolver;
import se.hiq.oss.kafka.streams.method.parameter.NoArgsParameterResolver;
import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

@SuppressWarnings("checkstyle:hiddenfield")
public class FlatMapValuesBuilder<V, VR> {

    private Object pojo;
    private String methodName;
    private Class<V> valueClass;
    private MethodUtils methodUtils = new MethodUtils();

    public FlatMapValuesBuilder(@NotNull final Object pojo) {
        this.pojo = pojo;
    }

    /**
     * Builds a KeyValueMapper by resolving a single annotated method.
     *
     * @return KeyValueMapper for the method found
     *
     * @throws IllegalStateException    If <i>methodName</i>,<i>key</i>, <i>to</i> or <i>from</i>
     *                                  provided can not be used to resolve a method.
     * @throws IllegalArgumentException If a single method could not be resolved.
     **/
    public ValueMapper<V, Iterable<VR>> build() {

        Method method = getSingleMethod();
        ParameterResolver parameterResolver = new NoArgsParameterResolver();
        if (method.getParameterTypes().length == 1) {
            parameterResolver = new DefaultParameterResolver();
        }
        return new ValueMapperMethod<>(pojo, method, parameterResolver, FlatMapValues.class);
    }


    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(FlatMapValues.class)
                .numberOfParameters(0).or().numberOfParameters(1)
                .returnType(Iterable.class);


        if (!StringUtils.isEmpty(methodName)) {
            methodFilterBuilder.name(methodName);
        }


        if (valueClass != null) {
            methodFilterBuilder.hasSignature(valueClass);
        } else {
            methodFilterBuilder.numberOfParameters(0).or().numberOfParameters(1);
        }


        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);
        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();

    }



    public FlatMapValuesBuilder<V, VR> from(Class<V> valueClass) {
        this.valueClass = valueClass;
        return this;
    }

    public FlatMapValuesBuilder<V, VR> methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }



}
