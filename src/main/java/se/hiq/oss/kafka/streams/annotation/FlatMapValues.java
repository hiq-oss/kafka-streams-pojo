package se.hiq.oss.kafka.streams.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a zero or one parameter method with {@code Iterable<V>} return type as a FlatMapValues method.
 *
 * Note: annotating a method with more parameters or with void return type will not have any effect.
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FlatMapValues {
}
