package se.hiq.oss.kafka.streams.method.parameter;

public class NoArgsParameterResolver implements ParameterResolver {
    @Override
    public Object[] resolveParameters(Object... parameters) {
        return new Object[0];
    }
}
