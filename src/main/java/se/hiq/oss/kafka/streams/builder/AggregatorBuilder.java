package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.Initializer;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.annotation.AggregatedValue;
import se.hiq.oss.kafka.streams.annotation.Aggregator;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;
import se.hiq.oss.kafka.streams.method.AggregatorMethod;
import se.hiq.oss.kafka.streams.method.InitializerMethod;
import se.hiq.oss.kafka.streams.method.parameter.AnnotationParameterResolver;
import se.hiq.oss.kafka.streams.method.parameter.NoArgsParameterResolver;

@SuppressWarnings("checkstyle:hiddenfield")
public class AggregatorBuilder<K, V, VA> {
    private static final int MAX_NUMBER_OF_PARAMETERS = 3;
    private Object pojo;
    private String methodName;
    private Class<K> keyClass;
    private Class<V> valueClass;
    private Class<VA> aggregateValueClass;
    private MethodUtils methodUtils = new MethodUtils();

    public AggregatorBuilder(@NotNull final Object pojo) {
       this.pojo = pojo;
    }

    public AggregatorMethod<K, V, VA> build() {

        Method method = getSingleMethod();

        return new AggregatorMethod<>(pojo,
                                       method,
                                       new AnnotationParameterResolver(method, Key.class, Value.class, AggregatedValue.class));
    }

    public Initializer<VA> buildInitializer() {
        Method method = getSingleInitializerMethod();
        return new InitializerMethod<>(pojo,
                method,
                new NoArgsParameterResolver());
    }




    private Method getSingleInitializerMethod() {
        Method method = getSingleMethod();
        Aggregator aggregatorAnnotation =  method.getAnnotation(Aggregator.class);
        String initializeMethodName = aggregatorAnnotation.initializerMethod();
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());

        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .numberOfParameters(0)
                .name(initializeMethodName);

        if (aggregateValueClass != null) {
            methodFilterBuilder.returnType(aggregateValueClass);
        } else {
            methodFilterBuilder.not().returnType(Void.TYPE);
        }

        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);

        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        Method initializerMethod = methods.iterator().next();
        validateInitializeMethod(initializerMethod, method);
        return initializerMethod;
    }

    private void validateInitializeMethod(Method initializeMethodName, Method method) {
        if (!initializeMethodName.getReturnType().equals(method.getReturnType())) {
            throw new IllegalArgumentException("The initializer method " + method
                    + " must have the same return type as " + method);
        }
    }


    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(Aggregator.class);


        methodUtils.filterOnNameAndParameters(methodName,
                methodFilterBuilder,
                MAX_NUMBER_OF_PARAMETERS,
                keyClass,
                valueClass,
                aggregateValueClass);

        if (aggregateValueClass != null) {
            methodFilterBuilder.returnType(aggregateValueClass);
        } else {
            methodFilterBuilder.not().returnType(Void.TYPE);
        }

        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);
        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();
    }



    public AggregatorBuilder<K, V, VA> methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }


    public AggregatorBuilder<K, V, VA> value(Class<V> valueClass) {
        this.valueClass = valueClass;
        return this;
    }

    public AggregatorBuilder<K, V, VA> key(Class<K> keyClass) {
        this.keyClass = keyClass;
        return this;
    }

    public AggregatorBuilder<K, V, VA> aggregateValue(Class<VA> aggregateValueClass) {
        this.aggregateValueClass = aggregateValueClass;
        return this;
    }
}
