package se.hiq.oss.kafka.streams.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks method with parameters annotated with @Key, @Value and @AggregatedValue and a non-void return type as a Aggregator method.
 *
 * <p>
 * Note: annotating a method with more parameters or with void return type will not have any effect.
 *
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aggregator {
    /* Set the initializer method, which returns the same type as the @AggregatedValue parameter and has no arguments */
    String initializerMethod();
}
