package se.hiq.oss.kafka.streams.method;


import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.ValueMapper;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

public class ValueMapperMethod<V, VR> implements ValueMapper<V, VR> {


    private Object pojo;
    private MethodInvoker methodInvoker;
    private ParameterResolver parameterResolver;

    public ValueMapperMethod(@NotNull final Object pojo,
                             @NotNull final Method method,
                             @NotNull final ParameterResolver parameterResolver,
                             @NotNull final Class<? extends Annotation> operatorAnnotationClass) {
        this.pojo = pojo;
        this.methodInvoker = new MethodInvoker(method, operatorAnnotationClass);
        this.parameterResolver = parameterResolver;
    }

    @Override
    public VR apply(V value) {
       return (VR) methodInvoker.invoke(pojo, parameterResolver.resolveParameters(value));
    }
}
