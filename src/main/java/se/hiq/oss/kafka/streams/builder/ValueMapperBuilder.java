package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.kstream.ValueMapper;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.ClassFilterBuilder;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.method.ValueMapperMethod;
import se.hiq.oss.kafka.streams.method.parameter.DefaultParameterResolver;
import se.hiq.oss.kafka.streams.method.parameter.NoArgsParameterResolver;
import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;



/**
 * Builds a ValueMapper by resolving a single @ValueMapper annotated method from a POJO.
 *
 * Methods can be resolved by:
 * <ul>
 *     <li>By method name (methodName)</li>
 *     <li>By types (from and to)</li>
 *     <li>By method name and types (methodName, from and to)</li>
 * </ul>
 *
 * Note: Only methods with non-void return type and one argument is considered.
 *
 * @param <V> Value Class (from)
 * @param <VR> Transformed Value Class (to)
 */
@SuppressWarnings("checkstyle:hiddenfield")
public class ValueMapperBuilder<V, VR>  {
    private Object pojo;
    private Class<V> fromValueClass;
    private Class<VR> toValueClass;
    private String methodName;
    private MethodUtils methodUtils = new MethodUtils();

    public ValueMapperBuilder(@NotNull final Object pojo) {
       this.pojo = pojo;
    }

    /**
     * Builds a ValueMapper by resolving a single annotated method.
     *
     * @return ValueMapper for the method found
     *
     * @throws IllegalStateException    If <i>methodName</i>, <i>to</i> or <i>from</i> provided can not be used to resolve a method.
     * @throws IllegalArgumentException If a single method could not be resolved.
     **/
    public ValueMapper<V, VR> build() {
        Method method = getSingleMethod();
        ParameterResolver parameterResolver = new NoArgsParameterResolver();
        if (method.getParameterTypes().length == 1) {
            parameterResolver = new DefaultParameterResolver();
        }
        return new ValueMapperMethod<>(pojo, method, parameterResolver, se.hiq.oss.kafka.streams.annotation.ValueMapper.class);
    }




    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(se.hiq.oss.kafka.streams.annotation.ValueMapper.class)
                .numberOfParameters(0).or().numberOfParameters(1);

        if (!StringUtils.isEmpty(methodName)) {
            methodFilterBuilder.name(methodName);
        }


        if (fromValueClass != null) {
            methodFilterBuilder
                    .parameterTypeFilter(0, new ClassFilterBuilder().extendsType(fromValueClass).build());
        } else {
            methodFilterBuilder.numberOfParameters(0).or().numberOfParameters(1);
        }

        if (toValueClass != null) {
            methodFilterBuilder
                    .returnTypeExtends(toValueClass);
        } else {
            methodFilterBuilder.not().returnType(Void.TYPE);
        }

        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);
        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();
    }



    public ValueMapperBuilder<V, VR> methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }

    public ValueMapperBuilder<V, VR> from(Class<V> fromValueClass) {
        this.fromValueClass = fromValueClass;
        return this;
    }

    public ValueMapperBuilder<V, VR> to(Class<VR> toValueClass) {
        this.toValueClass = toValueClass;
        return this;
    }
}
