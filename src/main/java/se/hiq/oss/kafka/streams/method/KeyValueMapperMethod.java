package se.hiq.oss.kafka.streams.method;


import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.KeyValueMapper;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;


public class KeyValueMapperMethod<K, V, VR> implements KeyValueMapper<K, V, VR> {


    private Object pojo;
    private ParameterResolver parameterResolver;
    private MethodInvoker methodInvoker;

    public KeyValueMapperMethod(@NotNull final Object pojo,
                                @NotNull final Method method,
                                @NotNull final ParameterResolver parameterResolver,
                                @NotNull final Class<? extends Annotation> operatorAnnotationClass) {
        this.pojo = pojo;
        this.parameterResolver = parameterResolver;
        this.methodInvoker = new MethodInvoker(method, operatorAnnotationClass);
    }

    @Override
    public VR apply(K key, V value) {
        return (VR) methodInvoker.invoke(pojo, parameterResolver.resolveParameters(key, value));
    }
}
