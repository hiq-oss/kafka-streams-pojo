package se.hiq.oss.kafka.streams;

import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Aggregator;
import org.apache.kafka.streams.kstream.Initializer;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Reducer;
import org.apache.kafka.streams.kstream.SessionWindowedKStream;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.TimeWindowedKStream;
import org.apache.kafka.streams.kstream.Window;
import org.apache.kafka.streams.kstream.Windows;
import org.apache.kafka.streams.state.KeyValueStore;

import se.hiq.oss.kafka.streams.builder.AggregatorBuilder;
import se.hiq.oss.kafka.streams.builder.ReducerBuilder;


public class PojoKGroupedStreamDecorator<K, V> implements PojoKGroupedStream<K, V> {

    private KGroupedStream<K, V> kGroupedStream;

    public PojoKGroupedStreamDecorator(final KGroupedStream<K, V> kGroupedStream) {
        this.kGroupedStream = kGroupedStream;
    }


    @Override
    public KTable<K, V> reduce(final Object pojo,
                               final String reducerMethodName,
                               final Class<V> valueType) {
        return reduce(new ReducerBuilder<V>(pojo).methodName(reducerMethodName).value(valueType).build());
    }

    @Override
    public KTable<K, V> reduce(final Object pojo,
                        final String reducerMethodName,
                        final Class<V> valueType,
                        final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return reduce(new ReducerBuilder<V>(pojo).methodName(reducerMethodName).value(valueType).build(),
                      materialized);
    }


    @Override
    public <VR> KTable<K, VR> aggregate(final Object pojo,
                                 final String aggregatorMethodName,
                                 final Class<K> keyType,
                                 final Class<V> valueType,
                                 final Class<VR> aggregatedValueType,
                                 final Materialized<K, VR, KeyValueStore<Bytes, byte[]>> materialized) {
        AggregatorBuilder<K, V, VR> aggregatorBuilder = new AggregatorBuilder<K, V, VR>(pojo).key(keyType).value(valueType).aggregateValue(aggregatedValueType);
        return aggregate(aggregatorBuilder.buildInitializer(), aggregatorBuilder.build(), materialized);
    }

    @Override
    public <VR> KTable<K, VR> aggregate(final Object pojo,
                                 final String aggregatorMethodName,
                                 final Class<K> keyType,
                                 final Class<V> valueType,
                                 final Class<VR> aggregatedValueType) {
        AggregatorBuilder<K, V, VR> aggregatorBuilder = new AggregatorBuilder<K, V, VR>(pojo).key(keyType).value(valueType).aggregateValue(aggregatedValueType);
        return aggregate(aggregatorBuilder.buildInitializer(), aggregatorBuilder.build());
    }




    @Override
    public KTable<K, Long> count() {
        return kGroupedStream.count();
    }



    @Override
    public KTable<K, Long> count(Materialized<K, Long, KeyValueStore<Bytes, byte[]>> materialized) {
        return kGroupedStream.count(materialized);
    }



    @Override
    public KTable<K, V> reduce(Reducer<V> reducer) {
        return kGroupedStream.reduce(reducer);
    }


    @Override
    public KTable<K, V> reduce(Reducer<V> reducer, Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return kGroupedStream.reduce(reducer, materialized);
    }


    @Override
    public <VR> KTable<K, VR> aggregate(Initializer<VR> initializer, Aggregator<? super K, ? super V, VR> aggregator, Materialized<K, VR, KeyValueStore<Bytes, byte[]>> materialized) {
        return kGroupedStream.aggregate(initializer, aggregator, materialized);
    }

    @Override
    public <VR> KTable<K, VR> aggregate(Initializer<VR> initializer, Aggregator<? super K, ? super V, VR> aggregator) {
        return kGroupedStream.aggregate(initializer, aggregator);
    }


    @Override
    public <W extends Window> TimeWindowedKStream<K, V> windowedBy(Windows<W> windows) {
        return kGroupedStream.windowedBy(windows);
    }

    @Override
    public SessionWindowedKStream<K, V> windowedBy(SessionWindows windows) {
        return kGroupedStream.windowedBy(windows);
    }
}
