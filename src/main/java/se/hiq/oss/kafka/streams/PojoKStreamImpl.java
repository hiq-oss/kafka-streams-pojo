package se.hiq.oss.kafka.streams;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.ForeachAction;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Serialized;
import org.apache.kafka.streams.kstream.ValueJoiner;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.apache.kafka.streams.kstream.internals.InternalStreamsBuilder;
import org.apache.kafka.streams.kstream.internals.KStreamImpl;

import se.hiq.oss.kafka.streams.builder.FlatMapBuilder;
import se.hiq.oss.kafka.streams.builder.FlatMapValuesBuilder;
import se.hiq.oss.kafka.streams.builder.ForeachBuilder;
import se.hiq.oss.kafka.streams.builder.MapBuilder;
import se.hiq.oss.kafka.streams.builder.PredicateBuilder;
import se.hiq.oss.kafka.streams.builder.SelectKeyBuilder;
import se.hiq.oss.kafka.streams.builder.ValueJoinerBuilder;
import se.hiq.oss.kafka.streams.builder.ValueMapperBuilder;

public class PojoKStreamImpl<K, V> extends KStreamImpl<K, V> implements PojoKStream<K, V> {

    public PojoKStreamImpl(final InternalStreamsBuilder builder,
                           final String name,
                           final Set<String> sourceNodes,
                           final boolean repartitionRequired) {
        super(builder, name, sourceNodes, repartitionRequired);
    }

    public PojoKStreamImpl(final KStream<K, V> kStream) {
        this(KStreamUtils.getBuilder((KStreamImpl) kStream),
                KStreamUtils.getName((KStreamImpl) kStream),
                KStreamUtils.getSourceNodes((KStreamImpl) kStream),
                KStreamUtils.isRepartitionRequired((KStreamImpl) kStream));
    }


    @Override
    public PojoKStream<K, V> filter(Object pojo,
                                    String predicateMethodName) {
        return filter(new PredicateBuilder<K, V>(pojo)
                .methodName(predicateMethodName)
                .build());
    }


    @Override
    public PojoKStream<K, V> filterNot(final Object pojo,
                                       final String predicateMethodName) {
        return filterNot(new PredicateBuilder<K, V>(pojo)
                .methodName(predicateMethodName)
                .build());
    }

    @Override
    public <KR> PojoKStream<KR, V> selectKey(final Object pojo,
                                             final String selectKeyMethodName,
                                             final Class<KR> toKeyClass) {
        return selectKey(new SelectKeyBuilder<K, V, KR>(pojo)
                .methodName(selectKeyMethodName)
                .to(toKeyClass)
                .build());
    }

    @Override
    public <KR, VR> PojoKStream<KR, VR> map(final Object pojo,
                                            final String mapMethodName,
                                            final Class<KR> toKeyClass,
                                            final Class<VR> toValueClass) {
        return map(new MapBuilder<K, V, KR, VR>(pojo)
                .methodName(mapMethodName)
                .build());
    }


    @Override
    public <VR> PojoKStream<K, VR> mapValues(final Object pojo,
                                             final String mapValuesMethodName,
                                             final Class<VR> toValueClass) {
        return mapValues(new ValueMapperBuilder<V, VR>(pojo)
                .methodName(mapValuesMethodName)
                .to(toValueClass)
                .build());
    }

    @Override
    public <KR, VR> PojoKStream<KR, VR> flatMap(final Object pojo,
                                                final String flatMapMethodName,
                                                final Class<KR> toKeyClass,
                                                final Class<VR> toValueClass) {
        return flatMap(new FlatMapBuilder<K, V, KR, VR>(pojo)
                .methodName(flatMapMethodName)
                .build());
    }

    @Override
    public <VR> PojoKStream<K, VR> flatMapValues(final Object pojo,
                                                 final String flatMapValuesMethodName,
                                                 final Class<VR> toValueClass) {
        return flatMapValues(new FlatMapValuesBuilder<V, VR>(pojo)
                .methodName(flatMapValuesMethodName)
                .build());
    }

    @Override
    public void foreach(final Object pojo,
                        final String forEachMethodName) {
        foreach(new ForeachBuilder<K, V>(pojo)
                .methodName(forEachMethodName)
                .build());
    }

    @Override
    public PojoKStream<K, V> peek(final Object pojo,
                                  final String forEachMethodName) {
        return peek(new ForeachBuilder<K, V>(pojo)
                .methodName(forEachMethodName)
                .build());
    }

    @Override
    public Map<String, PojoKStream<K, V>> branch(final Object pojo,
                                                 final String... predicateMethodNames) {
        Map<String, PojoKStream<K, V>> streamMap = new ConcurrentHashMap<>();

        PojoKStream<K, V>[] streams = branch(
                Stream.of(predicateMethodNames)
                      .map(n -> new PredicateBuilder<>(pojo).methodName(n).build())
                      .toArray(Predicate[]::new));

        for (int i = 0; i < predicateMethodNames.length; i++) {
            streamMap.put(predicateMethodNames[i], streams[i]);
        }
        return streamMap;
    }

    @Override
    public <KR> PojoKGroupedStream<KR, V> groupBy(final Object pojo,
                                                  final String selectKeyMethodName,
                                                  final Class<KR> toKeyClass) {
        return groupBy(new SelectKeyBuilder<K, V, KR>(pojo)
                .methodName(selectKeyMethodName)
                .build());
    }


    @Override
    public <KR> PojoKGroupedStream<KR, V> groupBy(final Object pojo,
                                                  final String selectKeyMethodName,
                                                  final Class<KR> toKeyClass,
                                                  final Serialized<KR, V> serialized) {
        return groupBy(new SelectKeyBuilder<K, V, KR>(pojo)
                .methodName(selectKeyMethodName)
                .to(toKeyClass)
                .build(), serialized);
    }


    @Override
    public <VO, VR> PojoKStream<K, VR> join(final KStream<K, VO> otherStream,
                                            final Object pojo,
                                            final String valueJoinerMethodName,
                                            final Class<VR> toValueClass,
                                            final JoinWindows windows) {
        return join(otherStream, new ValueJoinerBuilder<V, VO, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), windows);
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> join(final KStream<K, VO> otherStream,
                                            final Object pojo,
                                            final String valueJoinerMethodName,
                                            final Class<VR> toValueClass,
                                            final JoinWindows windows,
                                            final Joined<K, V, VO> joined) {
        return join(otherStream, new ValueJoinerBuilder<V, VO, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), windows, joined);
    }



    @Override
    public <VO, VR> PojoKStream<K, VR> leftJoin(final KStream<K, VO> otherStream,
                                                final Object pojo,
                                                final String valueJoinerMethodName,
                                                final Class<VR> toValueClass,
                                                final JoinWindows windows) {
        return leftJoin(otherStream, new ValueJoinerBuilder<V, VO, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), windows);
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> leftJoin(final KStream<K, VO> otherStream,
                                                final Object pojo,
                                                final String valueJoinerMethodName,
                                                final Class<VR> toValueClass,
                                                final JoinWindows windows,
                                                final Joined<K, V, VO> joined) {
        return leftJoin(otherStream, new ValueJoinerBuilder<V, VO, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), windows, joined);
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> outerJoin(final KStream<K, VO> otherStream,
                                                 final Object pojo,
                                                 final String valueJoinerMethodName,
                                                 final Class<VR> toValueClass,
                                                 final JoinWindows windows) {
        return outerJoin(otherStream, new ValueJoinerBuilder<V, VO, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), windows);
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> outerJoin(final KStream<K, VO> otherStream,
                                                 final Object pojo,
                                                 final String valueJoinerMethodName,
                                                 final Class<VR> toValueClass,
                                                 final JoinWindows windows,
                                                 final Joined<K, V, VO> joined) {
        return outerJoin(otherStream, new ValueJoinerBuilder<V, VO, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), windows, joined);
    }


    @Override
    public <VT, VR> PojoKStream<K, VR> join(final KTable<K, VT> table,
                                            final Object pojo,
                                            final String valueJoinerMethodName,
                                            final Class<VR> toValueClass) {
        return join(table, new ValueJoinerBuilder<V, VT, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build());
    }

    @Override
    public <VT, VR> PojoKStream<K, VR> join(final KTable<K, VT> table,
                                            final Object pojo,
                                            final String valueJoinerMethodName,
                                            final Class<VR> toValueClass,
                                            final Joined<K, V, VT> joined) {
        return join(table, new ValueJoinerBuilder<V, VT, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), joined);
    }



    @Override
    public <VT, VR> PojoKStream<K, VR> leftJoin(final KTable<K, VT> table,
                                                final Object pojo,
                                                final String valueJoinerMethodName,
                                                final Class<VR> toValueClass) {
        return leftJoin(table, new ValueJoinerBuilder<V, VT, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build());
    }

    @Override
    public <VT, VR> PojoKStream<K, VR> leftJoin(final KTable<K, VT> table,
                                                final Object pojo,
                                                final String valueJoinerMethodName,
                                                final Class<VR> toValueClass,
                                                final Joined<K, V, VT> joined) {
        return leftJoin(table, new ValueJoinerBuilder<V, VT, VR>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build(), joined);
    }



    @Override
    public <GK, GV, RV> PojoKStream<K, RV> join(final GlobalKTable<GK, GV> globalKTable,
                                                final Object pojo,
                                                final String selectKeyMethodName,
                                                final String valueJoinerMethodName,
                                                final Class<RV> toValueClass) {
        return join(globalKTable, new SelectKeyBuilder<K, V, GK>(pojo)
                .methodName(selectKeyMethodName)
                .build(), new ValueJoinerBuilder<V, GV, RV>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build());
    }

    @Override
    public <GK, GV, RV> PojoKStream<K, RV> leftJoin(final GlobalKTable<GK, GV> globalKTable,
                                                    final Object pojo,
                                                    final String selectKeyMethodName,
                                                    final String valueJoinerMethodName,
                                                    final Class<RV> toValueClass) {
        return leftJoin(globalKTable, new SelectKeyBuilder<K, V, GK>(pojo)
                .methodName(selectKeyMethodName)
                .build(), new ValueJoinerBuilder<V, GV, RV>(pojo)
                .methodName(valueJoinerMethodName)
                .to(toValueClass)
                .build());
    }


    @Override
    public PojoKStream<K, V> filter(final Predicate<? super K, ? super V> predicate) {
        return new PojoKStreamImpl<>(super.filter(predicate));
    }

    @Override
    public PojoKStream<K, V> filterNot(final Predicate<? super K, ? super V> predicate) {
        return new PojoKStreamImpl<>(super.filterNot(predicate));
    }

    @Override
    public <K1> PojoKStream<K1, V> selectKey(final KeyValueMapper<? super K, ? super V, ? extends K1> mapper) {
        return new PojoKStreamImpl<>(super.selectKey(mapper));
    }

    @Override
    public <K1, V1> PojoKStream<K1, V1> map(
            final KeyValueMapper<? super K, ? super V, ? extends KeyValue<? extends K1, ? extends V1>> mapper) {
        return new PojoKStreamImpl<>(super.map(mapper));
    }

    @Override
    public <V1> PojoKStream<K, V1> mapValues(final ValueMapper<? super V, ? extends V1> mapper) {
        return new PojoKStreamImpl<>(super.mapValues(mapper));
    }

    @Override
    public <K1, V1> PojoKStream<K1, V1> flatMap(
            final KeyValueMapper<? super K, ? super V, ? extends Iterable<? extends KeyValue<? extends K1, ? extends V1>>> mapper) {
        return new PojoKStreamImpl<>(super.flatMap(mapper));
    }

    @Override
    public <V1> PojoKStream<K, V1> flatMapValues(final ValueMapper<? super V, ? extends Iterable<? extends V1>> mapper) {
        return new PojoKStreamImpl<>(super.flatMapValues(mapper));
    }

    @Override
    public PojoKStream<K, V>[] branch(final Predicate<? super K, ? super V>... predicates) {

        return Stream.of(super.branch(predicates))
                     .map(p -> new PojoKStreamImpl<>(p))
                     .toArray(PojoKStreamImpl[]::new);
    }


    @Override
    public <K1> PojoKGroupedStream<K1, V> groupBy(final KeyValueMapper<? super K, ? super V, K1> selector) {
        return new PojoKGroupedStreamDecorator<>(super.groupBy(selector));
    }

    @Override
    public <KR> PojoKGroupedStream<KR, V> groupBy(final KeyValueMapper<? super K, ? super V, KR> selector,
                                                  final Serialized<KR, V> serialized) {
        return new PojoKGroupedStreamDecorator<>(super.groupBy(selector, serialized));
    }


    @Override
    public PojoKGroupedStream<K, V> groupByKey() {
        return new PojoKGroupedStreamDecorator<>(super.groupByKey());
    }

    @Override
    public PojoKGroupedStream<K, V> groupByKey(final Serialized<K, V> serialized) {
        return new PojoKGroupedStreamDecorator<>(super.groupByKey(serialized));
    }



    @Override
    public PojoKStream<K, V> merge(final KStream<K, V> stream) {
        return new PojoKStreamImpl<>(super.merge(stream));
    }


    @Override
    public PojoKStream<K, V> through(final String topic, final Produced<K, V> produced) {
        return new PojoKStreamImpl<>(super.through(topic, produced));
    }

    @Override
    public PojoKStream<K, V> peek(final ForeachAction<? super K, ? super V> action) {
        return new PojoKStreamImpl<>(super.peek(action));
    }

    @Override
    public PojoKStream<K, V> through(final String topic) {
        return new PojoKStreamImpl<>(super.through(topic));
    }

    @Override
    public <V1, R> PojoKStream<K, R> join(final KStream<K, V1> other,
                                      final ValueJoiner<? super V, ? super V1, ? extends R> joiner,
                                      final JoinWindows windows) {
        return new PojoKStreamImpl<>(super.join(other, joiner, windows));
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> join(final KStream<K, VO> otherStream,
                                        final ValueJoiner<? super V, ? super VO, ? extends VR> joiner,
                                        final JoinWindows windows,
                                        final Joined<K, V, VO> joined) {
        return new PojoKStreamImpl<>(super.join(otherStream, joiner, windows, joined));
    }

    @Override
    public <V1, R> PojoKStream<K, R> outerJoin(final KStream<K, V1> other,
                                           final ValueJoiner<? super V, ? super V1, ? extends R> joiner,
                                           final JoinWindows windows) {
        return new PojoKStreamImpl<>(super.outerJoin(other, joiner, windows));
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> outerJoin(final KStream<K, VO> other,
                                             final ValueJoiner<? super V, ? super VO, ? extends VR> joiner,
                                             final JoinWindows windows,
                                             final Joined<K, V, VO> joined) {
        return new PojoKStreamImpl<>(super.outerJoin(other, joiner, windows, joined));
    }


    @Override
    public <V1, R> PojoKStream<K, R> leftJoin(final KStream<K, V1> other,
                                          final ValueJoiner<? super V, ? super V1, ? extends R> joiner,
                                          final JoinWindows windows) {
        return new PojoKStreamImpl<>(super.leftJoin(other, joiner, windows));
    }

    @Override
    public <VO, VR> PojoKStream<K, VR> leftJoin(final KStream<K, VO> other,
                                            final ValueJoiner<? super V, ? super VO, ? extends VR> joiner,
                                            final JoinWindows windows,
                                            final Joined<K, V, VO> joined) {
        return new PojoKStreamImpl<>(super.leftJoin(other, joiner, windows, joined));
    }

    @Override
    public <V1, R> PojoKStream<K, R> join(final KTable<K, V1> other,
                                      final ValueJoiner<? super V, ? super V1, ? extends R> joiner) {
        return new PojoKStreamImpl<>(super.join(other, joiner));
    }

    @Override
    public <VT, VR> PojoKStream<K, VR> join(final KTable<K, VT> other,
                                        final ValueJoiner<? super V, ? super VT, ? extends VR> joiner,
                                        final Joined<K, V, VT> joined) {
        return new PojoKStreamImpl<>(super.join(other, joiner, joined));
    }

    @Override
    public <K1, V1, R> PojoKStream<K, R> leftJoin(final GlobalKTable<K1, V1> globalTable,
                                              final KeyValueMapper<? super K, ? super V, ? extends K1> keyMapper,
                                              final ValueJoiner<? super V, ? super V1, ? extends R> joiner) {
        return new PojoKStreamImpl<>(super.leftJoin(globalTable, keyMapper, joiner));
    }

    @Override
    public <K1, V1, V2> PojoKStream<K, V2> join(final GlobalKTable<K1, V1> globalTable,
                                            final KeyValueMapper<? super K, ? super V, ? extends K1> keyMapper,
                                            final ValueJoiner<? super V, ? super V1, ? extends V2> joiner) {
        return new PojoKStreamImpl<>(super.join(globalTable, keyMapper, joiner));
    }

    @Override
    public <V1, R> PojoKStream<K, R> leftJoin(final KTable<K, V1> other, final ValueJoiner<? super V, ? super V1, ? extends R> joiner) {
        return new PojoKStreamImpl<>(super.leftJoin(other, joiner));
    }

    @Override
    public <VT, VR> PojoKStream<K, VR> leftJoin(final KTable<K, VT> other,
                                            final ValueJoiner<? super V, ? super VT, ? extends VR> joiner,
                                            final Joined<K, V, VT> joined) {
        return new PojoKStreamImpl<>(super.leftJoin(other, joiner, joined));
    }
}
