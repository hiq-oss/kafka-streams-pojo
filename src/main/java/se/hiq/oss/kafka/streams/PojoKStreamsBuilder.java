package se.hiq.oss.kafka.streams;

import java.util.Collection;
import java.util.regex.Pattern;

import javax.validation.constraints.NotNull;

import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;


public class PojoKStreamsBuilder extends StreamsBuilder {

    private final StreamsBuilder streamsBuilder;


    public PojoKStreamsBuilder(@NotNull final StreamsBuilder streamsBuilder) {
        super();
        this.streamsBuilder = streamsBuilder;
    }


    private <K, V> PojoKStream<K, V> createStreamFrom(KStream<K, V> stream) {
        return new PojoKStreamImpl<>(stream);
    }

    @Override
    public <K, V> PojoKStream<K, V> stream(final String topic) {
        return createStreamFrom(streamsBuilder.<K, V>stream(topic));
    }

    @Override
    public <K, V> PojoKStream<K, V> stream(final String topic,
                                           final Consumed<K, V> consumed) {
        return createStreamFrom(streamsBuilder.stream(topic, consumed));
    }

    @Override
    public <K, V> PojoKStream<K, V> stream(final Collection<String> topics) {
        return createStreamFrom(streamsBuilder.<K, V>stream(topics));
    }

    @Override
    public <K, V> PojoKStream<K, V> stream(final Collection<String> topics,
                                           final Consumed<K, V> consumed) {
        return createStreamFrom(streamsBuilder.stream(topics, consumed));
    }

    @Override
    public <K, V> PojoKStream<K, V> stream(final Pattern topicPattern) {
        return createStreamFrom(streamsBuilder.<K, V>stream(topicPattern));
    }

    @Override
    public <K, V> PojoKStream<K, V> stream(final Pattern topicPattern,
                                                   Consumed<K, V> consumed) {
        return createStreamFrom(streamsBuilder.stream(topicPattern, consumed));
    }

    @Override
    public <K, V> KTable<K, V> table(final String topic,
                                     final Consumed<K, V> consumed,
                                     final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return streamsBuilder.table(topic, consumed, materialized);
    }

    @Override
    public <K, V> KTable<K, V> table(final String topic) {
        return streamsBuilder.table(topic);
    }

    @Override
    public <K, V> KTable<K, V> table(final String topic,
                                     final Consumed<K, V> consumed) {
        return streamsBuilder.table(topic, consumed);
    }

    @Override
    public <K, V> KTable<K, V> table(final String topic,
                                     final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return streamsBuilder.table(topic, materialized);
    }

    @Override
    public <K, V> GlobalKTable<K, V> globalTable(final String topic,
                                                 final Consumed<K, V> consumed) {
        return streamsBuilder.globalTable(topic, consumed);
    }

    @Override
    public <K, V> GlobalKTable<K, V> globalTable(final String topic) {
        return streamsBuilder.globalTable(topic);
    }

    @Override
    public <K, V> GlobalKTable<K, V> globalTable(final String topic, Consumed<K, V> consumed,
                                                 final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return streamsBuilder.globalTable(topic, consumed, materialized);
    }

    @Override
    public <K, V> GlobalKTable<K, V> globalTable(final String topic,
                                                 final Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return streamsBuilder.globalTable(topic, materialized);
    }

    @Override
    public StreamsBuilder addStateStore(final StoreBuilder builder) {
        return streamsBuilder.addStateStore(builder);
    }

    @Override
    public StreamsBuilder addGlobalStore(final StoreBuilder storeBuilder,
                                         final String topic,
                                         final String sourceName, Consumed consumed,
                                         final String processorName,
                                         final ProcessorSupplier stateUpdateSupplier) {
        return streamsBuilder.addGlobalStore(storeBuilder, topic, sourceName, consumed, processorName, stateUpdateSupplier);
    }

    @Override
    public Topology build() {
        return streamsBuilder.build();
    }
}
