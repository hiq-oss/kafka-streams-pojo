package se.hiq.oss.kafka.streams.method;

import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.ValueJoiner;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

public class ValueJoinerMethod<V1, V2, VR> implements ValueJoiner<V1, V2, VR> {

    private MethodInvoker methodInvoker;
    private Object pojo;
    private ParameterResolver parameterResolver;

    public ValueJoinerMethod(@NotNull final Object pojo,
                             @NotNull final Method method,
                             @NotNull final ParameterResolver parameterResolver) {
        this.pojo = pojo;
        this.methodInvoker = new MethodInvoker(method, se.hiq.oss.kafka.streams.annotation.ValueJoiner.class);
        this.parameterResolver = parameterResolver;
    }



    @Override
    public VR apply(V1 leftValue, V2 joinedValue) {
        return (VR) methodInvoker.invoke(pojo, parameterResolver.resolveParameters(leftValue, joinedValue));

    }
}
