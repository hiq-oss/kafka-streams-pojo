package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.annotation.FlatMap;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;
import se.hiq.oss.kafka.streams.method.KeyValueMapperMethod;
import se.hiq.oss.kafka.streams.method.parameter.AnnotationParameterResolver;

@SuppressWarnings("checkstyle:hiddenfield")
public class FlatMapBuilder<K, V, KR, VR> {

    private Object pojo;
    private String methodName;
    private Class<K> keyClass;
    private Class<V> valueClass;
    private MethodUtils methodUtils = new MethodUtils();

    public FlatMapBuilder(@NotNull final Object pojo) {
        this.pojo = pojo;
    }

    /**
     * Builds a KeyValueMapper by resolving a single annotated method.
     *
     * @return KeyValueMapper for the method found
     *
     * @throws IllegalStateException    If <i>methodName</i>,<i>key</i>, <i>to</i> or <i>from</i>
     *                                  provided can not be used to resolve a method.
     * @throws IllegalArgumentException If a single method could not be resolved.
     **/
    public KeyValueMapper<K, V, Iterable<KeyValue<KR, VR>>> build() {

        Method method = getSingleMethod();

        return new KeyValueMapperMethod<>(pojo,
                method,
                new AnnotationParameterResolver(method, Key.class, Value.class),
                FlatMap.class);
    }



    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(FlatMap.class)
                .returnType(Iterable.class);


        methodUtils.filterOnNameAndParameters(methodName, methodFilterBuilder, 2, keyClass, valueClass);


        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);

        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();

    }


    public FlatMapBuilder<K, V, KR, VR> key(Class<K> keyClass) {
        this.keyClass = keyClass;
        return this;
    }

    public FlatMapBuilder<K, V, KR, VR> value(Class<V> valueClass) {
        this.valueClass = valueClass;
        return this;
    }

    public FlatMapBuilder<K, V, KR, VR> methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }



}
