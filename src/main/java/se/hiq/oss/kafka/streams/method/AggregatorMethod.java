package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.Aggregator;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;


public class AggregatorMethod<K, V, VA> implements Aggregator<K, V, VA> {

    private MethodInvoker methodInvoker;
    private Object pojo;
    private ParameterResolver parameterResolver;

    public AggregatorMethod(@NotNull final Object pojo,
                            @NotNull final Method method,
                            @NotNull final ParameterResolver parameterResolver) {
        this.pojo = pojo;
        this.parameterResolver = parameterResolver;
        this.methodInvoker = new MethodInvoker(method, se.hiq.oss.kafka.streams.annotation.Aggregator.class);
    }

    @Override
    public  VA apply(final K key, final V value, final VA aggregate) {
        return (VA) methodInvoker.invoke(pojo, parameterResolver.resolveParameters(key, value, aggregate));
    }
}
