package se.hiq.oss.kafka.streams.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a two parameter method with a KeyValue return type as a KeyValueMapper.
 * <p>
 * One of the parameters needs to be annotated with @Key to qualify the key parameter from the value parameter.
 *
 * <p>
 * Note: annotating a method with more (or fewer) parameters or with void return type will not have any effect.
 *
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface KeyValueMapper {
}
