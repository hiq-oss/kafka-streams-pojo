package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.kstream.Reducer;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.method.ReducerMethod;
import se.hiq.oss.kafka.streams.method.parameter.DefaultParameterResolver;


/**
 * Builds a Reducer by resolving a single @Reducer annotated method from a POJO.
 *
 * Methods can be resolved by:
 * <ul>
 *     <li>By method name (methodName)</li>
 *     <li>By type (value)</li>
 *     <li>By method name and type (methodName, value)</li>
 * </ul>
 *
 * Note: Only methods with non-void return type and one or two arguments are considered.
 *
 * @param <V> Value Class
 */
@SuppressWarnings("checkstyle:hiddenfield")
public class ReducerBuilder<V> {

    private Object pojo;
    private String methodName;
    private Class<V> valueClass;
    private MethodUtils methodUtils = new MethodUtils();

    public ReducerBuilder(@NotNull final Object pojo) {
        this.pojo = pojo;
    }

    /**
     * Builds a Predicate by resolving a single annotated method.
     *
     * @return Predicate for the method found
     *
     * @throws IllegalStateException    If methodName or keyClass or valueClass is not provided.
     * @throws IllegalArgumentException If a single method could not be resolved.
     **/
    public Reducer<V> build() {
        Method method = getSingleMethod();
        validateMethod(method);
        return new ReducerMethod<>(pojo, method, new DefaultParameterResolver());
    }

    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .numberOfParameters(2)
                .annotated(se.hiq.oss.kafka.streams.annotation.Reducer.class);

        if (!StringUtils.isEmpty(methodName)) {
            methodFilterBuilder.name(methodName);
        }
        if (valueClass != null) {
            methodFilterBuilder.parametersExtendsType(valueClass, valueClass)
                               .returnType(valueClass);
        } else {
            methodFilterBuilder.not().returnType(Void.TYPE);
        }


        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);
        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();

    }


    private void validateMethod(Method method) {
        if (!isValid(method)) {
            throw new IllegalArgumentException("The type of both arguments and the return type does not match for method: " + method);
        }
    }

    private boolean isValid(Method method) {
        return method.getParameterTypes()[0].equals(method.getParameterTypes()[1])
                && method.getParameterTypes()[0].equals(method.getReturnType());
    }

    public ReducerBuilder<V> methodName(String methodName) {
      this.methodName = methodName;
        return this;
    }


    public ReducerBuilder<V> value(Class<V> valueClass) {
        this.valueClass = valueClass;
        return this;
    }

}
