package se.hiq.oss.kafka.streams.builder;


import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.KeyValueMapper;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.annotation.Key;
import se.hiq.oss.kafka.streams.annotation.Value;
import se.hiq.oss.kafka.streams.method.KeyValueMapperMethod;
import se.hiq.oss.kafka.streams.method.parameter.AnnotationParameterResolver;


/**
 * Builds a KeyValueMapper by resolving a single @KeyValueMapper annotated method from a POJO.
 *
 * Methods can be resolved by:
 * <ul>
 *     <li>By method name (methodName)</li>
 *     <li>By types (key, from and to)</li>
 *     <li>By method name and types (methodName, key, from and to)</li>
 * </ul>
 *
 * Note: Only methods with non-void return type and two arguments are considered.
 *
 * @param <K> Key Class (key)
 * @param <V> Value Class (from)
 * @param <VR> Transformed Value Class (to)
 */
@SuppressWarnings("checkstyle:hiddenfield")
public class KeyValueMapperBuilder<K, V, VR>  {

    private Object pojo;
    private String methodName;
    private Class<V> valueClass;
    private Class<VR> toClass;
    private Class<K> keyClass;
    private Class<? extends Annotation> operatorAnnotationClass = se.hiq.oss.kafka.streams.annotation.KeyValueMapper.class;
    private MethodUtils methodUtils = new MethodUtils();


    public KeyValueMapperBuilder(@NotNull final Object pojo) {
        this.pojo = pojo;
    }

    public KeyValueMapperBuilder(@NotNull final Object pojo, @NotNull final Class<? extends Annotation> operatorAnnotationClass) {
        this.pojo = pojo;
        this.operatorAnnotationClass = operatorAnnotationClass;
    }

    /**
     * Builds a KeyValueMapper by resolving a single annotated method.
     *
     * @return KeyValueMapper for the method found
     *
     * @throws IllegalStateException    If <i>methodName</i>,<i>key</i>, <i>to</i> or <i>from</i>
     *                                  provided can not be used to resolve a method.
     * @throws IllegalArgumentException If a single method could not be resolved.
     **/
    public KeyValueMapper<K, V, VR> build() {

        Method method = getSingleMethod();

        return new KeyValueMapperMethod<>(pojo,
                method,
                new AnnotationParameterResolver(method, Key.class, Value.class),
                operatorAnnotationClass);
    }


    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(operatorAnnotationClass);


        methodUtils.filterOnNameAndParameters(methodName, methodFilterBuilder, 2, keyClass, valueClass);

        if (toClass != null) {
            methodFilterBuilder.returnType(toClass);
        } else {
            methodFilterBuilder.not().returnType(Void.TYPE);
        }

        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);
        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();

    }



    public KeyValueMapperBuilder<K, V, VR> methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }

    public KeyValueMapperBuilder<K, V, VR> value(Class<V> fromValueClass) {
        this.valueClass = fromValueClass;
        return this;
    }

    public KeyValueMapperBuilder<K, V, VR> to(Class<VR> toClass) {
        this.toClass = toClass;
        return this;
    }

    public KeyValueMapperBuilder<K, V, VR> key(Class<K> keyClass) {
        this.keyClass = keyClass;
        return this;
    }
}
