package se.hiq.oss.kafka.streams.method;

import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.ForeachAction;

import se.hiq.oss.kafka.streams.annotation.Foreach;
import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;


public class ForeachMethod<K, V> implements ForeachAction<K, V> {

    private MethodInvoker methodInvoker;
    private Object pojo;
    private ParameterResolver parameterResolver;

    public ForeachMethod(@NotNull final Object pojo,
                         @NotNull final Method method,
                         @NotNull final ParameterResolver parameterResolver) {
        this.pojo = pojo;
        this.methodInvoker = new MethodInvoker(method, Foreach.class);
        this.parameterResolver = parameterResolver;
    }

    @Override
    public void apply(K key, V value) {
        methodInvoker.invoke(pojo, parameterResolver.resolveParameters(key, value));
    }
}
