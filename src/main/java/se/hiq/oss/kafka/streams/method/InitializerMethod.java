package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.Initializer;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

public class InitializerMethod<VA> implements Initializer<VA> {

    private Object pojo;
    private MethodInvoker methodInvoker;
    private ParameterResolver parameterResolver;

    public InitializerMethod(@NotNull final Object pojo,
                             @NotNull final Method method,
                             @NotNull final ParameterResolver parameterResolver) {
        this.pojo = pojo;
        this.parameterResolver = parameterResolver;
        this.methodInvoker = new MethodInvoker(method, se.hiq.oss.kafka.streams.annotation.Predicate.class);
    }


    @Override
    public VA apply() {
        return (VA) methodInvoker.invoke(pojo, parameterResolver.resolveParameters());
    }

}
