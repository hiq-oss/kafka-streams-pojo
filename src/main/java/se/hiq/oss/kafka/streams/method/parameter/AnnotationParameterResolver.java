package se.hiq.oss.kafka.streams.method.parameter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import se.hiq.oss.commons.reflection.util.AnnotationUtils;

public class AnnotationParameterResolver implements ParameterResolver {

    private Class<? extends Annotation>[] annotations;
    private int numberOfArgs;
    private Map<Integer, Integer> paramIndexBound = new ConcurrentHashMap<>();
    private String annotationsDescription = "";

    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public AnnotationParameterResolver(final Method method,
                                       final Class<? extends Annotation>... annotations) {
        this.annotations = annotations;
        this.numberOfArgs = method.getParameterTypes().length;
        setAnnotationsDescription();
        resolveParameterIndex(method);
    }

    private void resolveParameterIndex(Method method) {

        for (int i = 0; i < annotations.length; i++) {
            int indexBound = AnnotationUtils.getAnnotatedParameterIndex(method, annotations[i]);
            if (indexBound != -1) {
               paramIndexBound.put(indexBound, i);
            }
        }

        if (numberOfArgs != paramIndexBound.size()) {

            throw new IllegalArgumentException("All parameters of method " + method + " needs to be annotated with any of "
                                               + annotationsDescription);

        }

    }

    private void setAnnotationsDescription() {
        annotationsDescription = Stream.of(annotations).map(a -> "@" + a.getName()).collect(Collectors.joining(","));
    }


    @SuppressWarnings("PMD.AvoidArrayLoops")
    @Override
    public Object[] resolveParameters(Object... parameters) {
        Object[] argsReturned = new Object[numberOfArgs];
        for (int i = 0; i < numberOfArgs; i++) {
            int index = paramIndexBound.get(i);
            argsReturned[i] = parameters[index];
        }
        return argsReturned;
    }
}
