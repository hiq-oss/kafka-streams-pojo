package se.hiq.oss.kafka.streams.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a zero, one or two parameter method with a {@code KeyValue<K, V>>} return type as a Map method.
 * <p>
 * Parameters needs to be annotated with @Key or @Value to bind parameter to either key or value.
 *
 * <p>
 * Note: annotating a method with more parameters or with void return type will not have any effect.
 *
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Map {
}
