package se.hiq.oss.kafka.streams.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a two parameter method with a non-void return type as a ValueJoiner
 *
 * <p>
 * Note: annotating a method with more (or fewer) parameters or with void return type will not have any effect.
 *
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValueJoiner {
}
