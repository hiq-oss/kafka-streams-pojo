package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.Reducer;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;


public class ReducerMethod<V> implements Reducer<V> {

    private MethodInvoker methodInvoker;
    private Object pojo;
    private ParameterResolver parameterResolver;

    public ReducerMethod(@NotNull final Object pojo,
                         @NotNull final Method method,
                         @NotNull final ParameterResolver parameterResolver) {
        this.pojo = pojo;
        this.parameterResolver = parameterResolver;
        this.methodInvoker = new MethodInvoker(method, se.hiq.oss.kafka.streams.annotation.Reducer.class);
    }

    @Override
    public V apply(V value1, V value2) {
        return (V) methodInvoker.invoke(pojo, parameterResolver.resolveParameters(value1, value2));
    }
}
