package se.hiq.oss.kafka.streams.builder;


import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.ValueJoiner;

import se.hiq.oss.commons.reflection.ClassIntrospector;
import se.hiq.oss.commons.reflection.ClassIntrospectorImpl;
import se.hiq.oss.commons.reflection.filter.MethodFilter;
import se.hiq.oss.commons.reflection.filter.builder.MethodFilterBuilder;
import se.hiq.oss.kafka.streams.annotation.JoinedValue;
import se.hiq.oss.kafka.streams.annotation.Value;
import se.hiq.oss.kafka.streams.method.ValueJoinerMethod;
import se.hiq.oss.kafka.streams.method.parameter.AnnotationParameterResolver;

@SuppressWarnings("checkstyle:hiddenfield")
public class ValueJoinerBuilder<V, VO, VR> {

    private Object pojo;
    private String methodName;
    private Class<V> valueClass;
    private Class<VO> joinedValueClass;
    private Class<VR> toValueClass;
    private MethodUtils methodUtils = new MethodUtils();

    public ValueJoinerBuilder(@NotNull final Object pojo) {
       this.pojo = pojo;
    }

    public ValueJoiner<V, VO, VR> build() {

        Method method = getSingleMethod();

        return new ValueJoinerMethod<>(pojo,
                                       method,
                                       new AnnotationParameterResolver(method, Value.class, JoinedValue.class));
    }


    private Method getSingleMethod() {
        ClassIntrospector classIntrospector = new ClassIntrospectorImpl(pojo.getClass());
        MethodFilterBuilder methodFilterBuilder = new MethodFilterBuilder()
                .isPublic()
                .annotated(se.hiq.oss.kafka.streams.annotation.ValueJoiner.class);


        methodUtils.filterOnNameAndParameters(methodName, methodFilterBuilder, 2, valueClass, joinedValueClass);

        if (toValueClass != null) {
            methodFilterBuilder.returnType(toValueClass);
        } else {
            methodFilterBuilder.not().returnType(Void.TYPE);
        }

        MethodFilter methodFilter = methodFilterBuilder.build();
        Set<Method> methods = classIntrospector.getMethods(methodFilter);
        methodUtils.validateSingleMethod(pojo, methods, methodFilter);

        return methods.iterator().next();
    }



    public ValueJoinerBuilder<V, VO, VR> methodName(String methodName) {
        this.methodName = methodName;
        return this;
    }


    public ValueJoinerBuilder<V, VO, VR> value(Class<V> valueClass) {
        this.valueClass = valueClass;
        return this;
    }

    public ValueJoinerBuilder<V, VO, VR> joinedValue(Class<VO> joinedValueClass) {
        this.joinedValueClass = joinedValueClass;
        return this;
    }

    public ValueJoinerBuilder<V, VO, VR>  to(Class<VR> toValueClass) {
        this.toValueClass = toValueClass;
        return this;
    }
}
