package se.hiq.oss.kafka.streams.method;


import java.lang.reflect.Method;

import javax.validation.constraints.NotNull;

import org.apache.kafka.streams.kstream.Predicate;

import se.hiq.oss.kafka.streams.method.parameter.ParameterResolver;

public class PredicateMethod<K, V> implements Predicate<K, V> {

    private Object pojo;
    private MethodInvoker methodInvoker;
    private ParameterResolver parameterResolver;

    public PredicateMethod(@NotNull final Object pojo,
                           @NotNull final Method method,
                           @NotNull final ParameterResolver parameterResolver) {
        this.pojo = pojo;
        this.parameterResolver = parameterResolver;
        this.methodInvoker = new MethodInvoker(method, se.hiq.oss.kafka.streams.annotation.Predicate.class);
    }


    @Override
    public boolean test(K key, V value) {
        return (boolean) methodInvoker.invoke(pojo, parameterResolver.resolveParameters(key, value));
    }

}
