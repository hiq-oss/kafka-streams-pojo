package se.hiq.oss.kafka.streams.method;


import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;

public class MethodInvoker {
    private static final String ERROR_MESSAGE = "Could not invoke @%s annotated method %s";

    private Method method;
    private Class<? extends Annotation> annotationClass;

    public MethodInvoker(final Method method, final Class<? extends Annotation> annotationClass) {
        this.method = method;
        this.annotationClass = annotationClass;
    }


    public Object invoke(Object object, Object... args) {
        try {
            return method.invoke(object, args);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(String.format(ERROR_MESSAGE, annotationClass.getName(), method), e);
        } catch (InvocationTargetException e) {
            throw new IllegalStateException(String.format(ERROR_MESSAGE, annotationClass.getName(), method), e.getCause()); // NOPMD
        } catch (IllegalArgumentException e) {
            throw new IllegalStateException(String.format(ERROR_MESSAGE, annotationClass.getName(), method)
                    + " using arguments (" + StringUtils.join(args, ", ") + ")", e);
        }
    }
}
