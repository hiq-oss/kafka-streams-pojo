package se.hiq.oss.kafka.streams;


import java.util.Map;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.ForeachAction;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Serialized;
import org.apache.kafka.streams.kstream.ValueJoiner;
import org.apache.kafka.streams.kstream.ValueMapper;



public interface PojoKStream<K, V> extends KStream<K, V> {
    /**
     * Create a new {@code PojoKStream} that consists of all records of this stream which satisfy the given predicate.
     * All records that do not satisfy the predicate are dropped.
     * This is a stateless record-by-record operation.
     *
     * @param pojo                The java object that implements the @Predicate annotated method
     * @param predicateMethodName Name of the @Predicate  annotated method to use as predicate function that is applied to each record
     * @return a {@code PojoKStream} that contains only those records that satisfy the given predicate
     * @see #filterNot(Predicate)
     */
    PojoKStream<K, V> filter(Object pojo,
                             String predicateMethodName);

    /**
     * Create a new {@code PojoKStream} that consists all records of this stream which do <em>not</em> satisfy the given
     * predicate.
     * All records that <em>do</em> satisfy the predicate are dropped.
     * This is a stateless record-by-record operation.
     *
     * @param pojo                The java object that implements the @Predicate annotated method
     * @param predicateMethodName Name of the @Predicate annotated method to use as predicate function that is applied to each record
     * @return a {@code PojoKStream} that contains only those records that do <em>not</em> satisfy the given predicate
     * @see #filter(Predicate)
     */
    PojoKStream<K, V> filterNot(Object pojo,
                                String predicateMethodName);


    /**
     * Set a new key (with possibly new type) for each input record.
     * The provided method with name <i>selectKeyMethodName</i> is applied to each input record and computes a new key for it.
     * Thus, an input record {@code <K,V>} can be transformed into an output record {@code <K':V>}.
     * This is a stateless record-by-record operation.
     * <p>
     * For example, you can use this transformation to set a key for a key-less input record {@code <null,V>} by
     * extracting a key from the value within your selectKeyMethod. The example below computes the new key as the
     * length of the value string.
     * <p>
     * Setting a new key might result in an internal data redistribution if a key based operator (like an aggregation or
     * join) is applied to the result {@code PojoKStream}.
     *
     * @param pojo                The java object that implements the @SelectKey annotated method
     * @param selectKeyMethodName Name of the @SelectKey annotated method that computes a new key for each record
     * @param toKeyClass          The class of the key that is used in the returned stream.
     * @param <KR>                the new key type of the result stream
     * @return a {@code PojoKStream} that contains records with new key (possibly of different type) and unmodified value
     * @see #map(KeyValueMapper)
     * @see #flatMap(KeyValueMapper)
     * @see #mapValues(ValueMapper)
     * @see #flatMapValues(ValueMapper)
     **/
    <KR> PojoKStream<KR, V> selectKey(Object pojo,
                                      String selectKeyMethodName,
                                      Class<KR> toKeyClass);

    /**
     * Transform each record of the input stream into a new record in the output stream (both key and value type can be
     * altered arbitrarily).
     * The provided method with name <i>mapMethodName</i> is applied to each input record and computes a new output record.
     * Thus, an input record {@code <K,V>} can be transformed into an output record {@code <K':V'>}.
     * This is a stateless record-by-record operation (cf. {@link #transform(org.apache.kafka.streams.kstream.TransformerSupplier, String...)} for
     * stateful record transformation).
     * <p>
     * The provided mapMethod must return a {@link KeyValue} type and must not return {@code null}.
     * <p>
     * Mapping records might result in an internal data redistribution if a key based operator (like an aggregation or
     * join) is applied to the result {@code PojoKStream}.
     *
     * @param pojo          The java object that implements the @Map annotated method
     * @param mapMethodName Name of the @Map annotated method that computes a new output record
     * @param toKeyClass    The class of the key that is used in the returned stream.
     * @param toValueClass  The class of the value that is used in the returned stream.
     * @param <KR>          the key type of the result stream
     * @param <VR>          the value type of the result stream
     * @return a {@code PojoKStream} that contains records with new key and value (possibly both of different type)
     * @see #selectKey(KeyValueMapper)
     * @see #flatMap(KeyValueMapper)
     * @see #mapValues(ValueMapper)
     * @see #flatMapValues(ValueMapper)
     * @see #transform(org.apache.kafka.streams.kstream.TransformerSupplier, String...)
     * @see #transformValues(org.apache.kafka.streams.kstream.ValueTransformerSupplier, String...)
     */
    <KR, VR> PojoKStream<KR, VR> map(Object pojo,
                                     String mapMethodName,
                                     Class<KR> toKeyClass,
                                     Class<VR> toValueClass);


    /**
     * Transform the value of each input record into a new value (with possible new type) of the output record.
     * The provided method with name <i>mapValuesMethodName</i> is applied to each input record value and computes a new value for it.
     * Thus, an input record {@code <K,V>} can be transformed into an output record {@code <K:V'>}.
     * This is a stateless record-by-record operation (cf.
     * {@link #transformValues(org.apache.kafka.streams.kstream.ValueTransformerSupplier, String...)} for stateful value transformation).
     * <p>
     * Setting a new value preserves data co-location with respect to the key.
     * Thus, <em>no</em> internal data redistribution is required if a key based operator (like an aggregation or join)
     * is applied to the result {@code PojoKStream}.
     *
     * @param pojo                The java object that implements the @ValueMapper annotated method
     * @param mapValuesMethodName Name of the @ValueMapper annotated method that computes a new output value
     * @param toValueClass        The class of the value that is used in the returned stream.
     * @param <VR>                the value type of the result stream
     * @return a {@code PojoKStream} that contains records with unmodified key and new values (possibly of different type)
     * @see #selectKey(KeyValueMapper)
     * @see #map(KeyValueMapper)
     * @see #flatMap(KeyValueMapper)
     * @see #flatMapValues(ValueMapper)
     * @see #transform(org.apache.kafka.streams.kstream.TransformerSupplier, String...)
     * @see #transformValues(org.apache.kafka.streams.kstream.ValueTransformerSupplier, String...)
     **/
    <VR> PojoKStream<K, VR> mapValues(Object pojo,
                                      String mapValuesMethodName,
                                      Class<VR> toValueClass);

    /**
     * Transform each record of the input stream into zero or more records in the output stream (both key and value type
     * can be altered arbitrarily).
     * The provided method with name <i>flatMapMethodName</i> is applied to each input record and computes zero or more output records.
     * Thus, an input record {@code <K,V>} can be transformed into output records {@code <K':V'>, <K'':V''>, ...}.
     * This is a stateless record-by-record operation (cf. {@link #transform(org.apache.kafka.streams.kstream.TransformerSupplier, String...)} for
     * stateful record transformation).
     * <p>
     * The provided flatMapMethod must return an {@link Iterable} (e.g., any {@link java.util.Collection} type)
     * and the return value must not be {@code null}.
     * <p>
     * Flat-mapping records might result in an internal data redistribution if a key based operator (like an aggregation
     * or join) is applied to the result {@code PojoKStream}.
     *
     * @param pojo              The java object that implements the @FlatMap annotated method
     * @param flatMapMethodName Name of the @FlatMap annotated method that computes the new output records
     * @param toKeyClass        The class of the key that is used in the returned stream.
     * @param toValueClass      The class of the value that is used in the returned stream.
     * @param <KR>              the key type of the result stream
     * @param <VR>              the value type of the result stream
     * @return a {@code PojoKStream} that contains more or less records with new key and value (possibly of different type)
     * @see #selectKey(KeyValueMapper)
     * @see #map(KeyValueMapper)
     * @see #mapValues(ValueMapper)
     * @see #flatMapValues(ValueMapper)
     * @see #transform(org.apache.kafka.streams.kstream.TransformerSupplier, String...)
     * @see #transformValues(org.apache.kafka.streams.kstream.ValueTransformerSupplier, String...)
     **/
    <KR, VR> PojoKStream<KR, VR> flatMap(Object pojo,
                                         String flatMapMethodName,
                                         Class<KR> toKeyClass,
                                         Class<VR> toValueClass);

    /**
     * Create a new {@code PojoKStream} by transforming the value of each record in this stream into zero or more values
     * with the same key in the new stream.
     * Transform the value of each input record into zero or more records with the same (unmodified) key in the output
     * stream (value type can be altered arbitrarily).
     * The provided  method with name <i>flatMapValuesMethodName</i> is applied to each input record and computes zero or more output values.
     * Thus, an input record {@code <K,V>} can be transformed into output records {@code <K:V'>, <K:V''>, ...}.
     * This is a stateless record-by-record operation.
     * for stateful value transformation).
     * <p>
     * The provided flatMapValuesMethod must return an {@link Iterable} (e.g., any {@link java.util.Collection} type)
     * and the return value must not be {@code null}.
     * <p>
     * Splitting a record into multiple records with the same key preserves data co-location with respect to the key.
     * Thus, <em>no</em> internal data redistribution is required if a key based operator (like an aggregation or join)
     * is applied to the result {@code KStream}.
     *
     * @param pojo                    The java object that implements the @FlatMapValues annotated method
     * @param flatMapValuesMethodName Name of the @FlatMapValues annotated method the computes the new output values
     * @param toValueClass            The class of the value that is used in the returned stream.
     * @param <VR>                    the value type of the result stream
     * @return a {@code PojoKStream} that contains more or less records with unmodified keys and new values of different type
     * @see #selectKey(KeyValueMapper)
     * @see #map(KeyValueMapper)
     * @see #flatMap(KeyValueMapper)
     * @see #mapValues(ValueMapper)
     * @see #transform(org.apache.kafka.streams.kstream.TransformerSupplier, String...)
     * @see #transformValues(org.apache.kafka.streams.kstream.ValueTransformerSupplier, String...)
     **/
    <VR> PojoKStream<K, VR> flatMapValues(Object pojo,
                                          String flatMapValuesMethodName,
                                          Class<VR> toValueClass);

    /**
     * Perform an action on each record of {@code PojoKStream}.
     * This is a stateless record-by-record operation (cf. {@link #process(org.apache.kafka.streams.processor.ProcessorSupplier, String...)}).
     * Note that this is a terminal operation that returns void.
     *
     * @param pojo              The java object that implements the @Foreach annotated method
     * @param forEachMethodName Name of the @Foreach annotated method to perform on each record
     * @see #process(org.apache.kafka.streams.processor.ProcessorSupplier, String...)
     */
    void foreach(Object pojo,
                 String forEachMethodName);

    /**
     * Perform an action on each record of {@code PojoKStream}.
     * This is a stateless record-by-record operation (cf. {@link #process(org.apache.kafka.streams.processor.ProcessorSupplier, String...)}).
     * <p>
     * Peek is a non-terminal operation that triggers a side effect (such as logging or statistics collection)
     * and returns an unchanged stream.
     * <p>
     * Note that since this operation is stateless, it may execute multiple times for a single record in failure cases.
     *
     * @param pojo              The java object that implements the @Foreach annotated method
     * @param forEachMethodName Name of the @Foreach annotated method to perform on each record
     * @return The original stream.
     * @see #process(org.apache.kafka.streams.processor.ProcessorSupplier, String...)
     */
    PojoKStream<K, V> peek(Object pojo,
                           String forEachMethodName);

    /**
     * Creates an array of {@code PojoKStream} from this stream by branching the records in the original stream based on
     * the supplied predicates.
     * Each record is evaluated against the supplied predicates, and predicates are evaluated in order.
     * Each stream in the result array corresponds position-wise (index) to the predicate in the supplied predicates.
     * The branching happens on first-match: A record in the original stream is assigned to the corresponding result
     * stream for the first predicate that evaluates to true, and is assigned to this stream only.
     * A record will be dropped if none of the predicates evaluate to true.
     * This is a stateless record-by-record operation.
     *
     * @param pojo                 The java object that implements the @Predicate annotated methods.
     * @param predicateMethodNames List of names of @Predicate annotated methods to branch the stream on
     * @return Map of multiple distinct substreams of this {@code PojoKStream} keyed by its methodName.
     **/
    Map<String, PojoKStream<K, V>> branch(Object pojo,
                                          String... predicateMethodNames);

    /**
     * Group the records of this {@code PojoKStream} on a new key that is selected using the provided {@link KeyValueMapper}
     * and default serializers and deserializers.
     * Grouping a stream on the record key is required before an aggregation operator can be applied to the data
     * (cf. {@link PojoKGroupedStream}).
     * The {@link KeyValueMapper} selects a new key (with should be of the same type) while preserving the original values.
     * If the new record key is {@code null} the record will not be included in the resulting {@link PojoKGroupedStream}
     * <p>
     * Because a new key is selected, an internal repartitioning topic will be created in Kafka.
     * This topic will be named "${applicationId}-XXX-repartition", where "applicationId" is user-specified in
     * {@link  org.apache.kafka.streams.StreamsConfig} via parameter {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is
     * an internally generated name, and "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * All data of this stream will be redistributed through the repartitioning topic by writing all records to it,
     * and rereading all records from it, such that the resulting {@link PojoKGroupedStream} is partitioned on the new key.
     * <p>
     * This operation is equivalent to calling {@link #selectKey(KeyValueMapper)} followed by {@link #groupByKey()}.
     * If the key type is changed, it is recommended to use {@link #groupBy(KeyValueMapper, Serialized)} instead.
     *
     * @param pojo                The java object that implements the @SelectKey annotated methods.
     * @param selectKeyMethodName List of names of @SelectKey annotated method that computes a new key for grouping
     * @param toKeyClass          Class of the key returned.
     * @param <KR>                the key type of the result {@link PojoKGroupedStream}
     * @return a {@link PojoKGroupedStream} that contains the grouped records of the original {@code PojoKStream}
     */
    <KR> PojoKGroupedStream<KR, V> groupBy(Object pojo,
                                           String selectKeyMethodName,
                                           Class<KR> toKeyClass);

    /**
     * Group the records of this {@code PojoKStream} on a new key that is selected using the provided {@link KeyValueMapper}
     * and {@link org.apache.kafka.common.serialization.Serde}s as specified by {@link Serialized}.
     * Grouping a stream on the record key is required before an aggregation operator can be applied to the data
     * (cf. {@link PojoKGroupedStream}).
     * The method named <i>selectKeyMethodName</i> selects a new key (with should be of the same type) while preserving the original values.
     * If the new record key is {@code null} the record will not be included in the resulting {@link PojoKGroupedStream}.
     * <p>
     * Because a new key is selected, an internal repartitioning topic will be created in Kafka.
     * This topic will be named "${applicationId}-XXX-repartition", where "applicationId" is user-specified in
     * {@link  org.apache.kafka.streams.StreamsConfig} via parameter {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is
     * an internally generated name, and "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * All data of this stream will be redistributed through the repartitioning topic by writing all records to it,
     * and rereading all records from it, such that the resulting {@link PojoKGroupedStream} is partitioned on the new key.
     * <p>
     * This operation is equivalent to calling {@link #selectKey(KeyValueMapper)} followed by {@link #groupByKey()}.
     *
     * @param pojo                The java object that implements the @SelectKey annotated methods.
     * @param selectKeyMethodName List of names of @SelectKey annotated method that computes a new key for grouping
     * @param toKeyClass          Class of the key returned.
     * @param serialized          Serialized to  be used.
     * @param <KR>                the key type of the result {@link PojoKGroupedStream}
     * @return a {@link PojoKGroupedStream} that contains the grouped records of the original {@code PojoKStream}
     **/
    <KR> PojoKGroupedStream<KR, V> groupBy(Object pojo,
                                           String selectKeyMethodName,
                                           Class<KR> toKeyClass,
                                           Serialized<KR, V> serialized);



    /**
     * Join records of this stream with another {@code PojoKStream}'s records using windowed inner equi join with default
     * serializers and deserializers.
     * The join is computed on the records' key with join attribute {@code thisKStream.key == otherKStream.key}.
     * Furthermore, two records are only joined if their timestamps are close to each other as defined by the given
     * {@link JoinWindows}, i.e., the window defines an additional join predicate on the record timestamps.
     * <p>
     * For each pair of records meeting both join predicates the provided method with name <i>valueJoinerMethodName</i> will be called to compute
     * a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * If an input record key or value is {@code null} the record will not be included in the join operation and thus no
     * output record will be added to the resulting {@code PojoKStream}.
     * <p>
     * Example (assuming all input records belong to the correct windows):
     * <table border='1'>
     * <tr>
     * <th>this</th>
     * <th>other</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>&lt;K2:B&gt;</td>
     * <td>&lt;K2:b&gt;</td>
     * <td>&lt;K2:ValueJoiner(B,b)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K3:c&gt;</td>
     * <td></td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} (for one input stream) before doing the
     * join, using a pre-created topic with the "correct" number of partitions.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner).
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link  org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen for one or both of the joining {@code KStream}s.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     * <p>
     * Both of the joining {@code KStream}s will be materialized in local state stores with auto-generated store names.
     * For failure and recovery each store will be backed by an internal changelog topic that will be created in Kafka.
     * The changelog topic will be named "${applicationId}-storeName-changelog", where "applicationId" is user-specified
     * in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "storeName" is an
     * internally generated name, and "-changelog" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     *
     * @param otherStream           the {@code KStream} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param windows               the specification of the {@link JoinWindows}
     * @param <VO>                  the value type of the other stream
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * {@link ValueJoiner}, one for each matched record-pair with the same key and within the joining window intervals
     * @see #leftJoin(KStream, ValueJoiner, JoinWindows)
     * @see #outerJoin(KStream, ValueJoiner, JoinWindows)
     **/
    <VO, VR> PojoKStream<K, VR> join(KStream<K, VO> otherStream,
                                     Object pojo,
                                     String valueJoinerMethodName,
                                     Class<VR> toValueClass,
                                     JoinWindows windows);

    /**
     * Join records of this stream with another {@code KStream}'s records using windowed inner equi join with default
     * serializers and deserializers.
     * The join is computed on the records' key with join attribute {@code thisKStream.key == otherKStream.key}.
     * Furthermore, two records are only joined if their timestamps are close to each other as defined by the given
     * {@link JoinWindows}, i.e., the window defines an additional join predicate on the record timestamps.
     * <p>
     * For each pair of records meeting both join predicates the provided method with name <i>valueJoinerMethodName</i> will be called to compute
     * a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * If an input record key or value is {@code null} the record will not be included in the join operation and thus no
     * output record will be added to the resulting {@code KStream}.
     * <p>
     * Example (assuming all input records belong to the correct windows):
     * <table border='1'>
     * <tr>
     * <th>this</th>
     * <th>other</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>&lt;K2:B&gt;</td>
     * <td>&lt;K2:b&gt;</td>
     * <td>&lt;K2:ValueJoiner(B,b)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K3:c&gt;</td>
     * <td></td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} (for one input stream) before doing the
     * join, using a pre-created topic with the "correct" number of partitions.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner).
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link  org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen for one or both of the joining {@code KStream}s.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     * <p>
     * Both of the joining {@code KStream}s will be materialized in local state stores with auto-generated store names.
     * For failure and recovery each store will be backed by an internal changelog topic that will be created in Kafka.
     * The changelog topic will be named "${applicationId}-storeName-changelog", where "applicationId" is user-specified
     * in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "storeName" is an
     * internally generated name, and "-changelog" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     *
     * @param otherStream           the {@code KStream} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param windows               the specification of the {@link JoinWindows}
     * @param joined                a {@link Joined} instance that defines the serdes to
     *                              be used to serialize/deserialize inputs and outputs of the joined streams
     * @param <VO>                  the value type of the other stream
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key and within the joining window intervals
     * @see #leftJoin(KStream, ValueJoiner, JoinWindows, Joined)
     * @see #outerJoin(KStream, ValueJoiner, JoinWindows, Joined)
     **/
    <VO, VR> PojoKStream<K, VR> join(final KStream<K, VO> otherStream,
                                     final Object pojo,
                                     final String valueJoinerMethodName,
                                     final Class<VR> toValueClass,
                                     final JoinWindows windows,
                                     final Joined<K, V, VO> joined);



    /**
     * Join records of this stream with another {@code KStream}'s records using windowed left equi join with default
     * serializers and deserializers.
     * In contrast to {@link #join(KStream, ValueJoiner, JoinWindows) inner-join}, all records from this stream will
     * produce at least one output record (cf. below).
     * The join is computed on the records' key with join attribute {@code thisKStream.key == otherKStream.key}.
     * Furthermore, two records are only joined if their timestamps are close to each other as defined by the given
     * {@link JoinWindows}, i.e., the window defines an additional join predicate on the record timestamps.
     * <p>
     * For each pair of records meeting both join predicates the provided method with name <i>valueJoinerMethodName</i> will be called to compute
     * a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * Furthermore, for each input record of this {@code KStream} that does not satisfy the join predicate the provided
     * {@link ValueJoiner} will be called with a {@code null} value for the other stream.
     * If an input record key or value is {@code null} the record will not be included in the join operation and thus no
     * output record will be added to the resulting {@code PojoKStream}.
     * <p>
     * Example (assuming all input records belong to the correct windows):
     * <table border='1'>
     * <tr>
     * <th>this</th>
     * <th>other</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td>&lt;K1:ValueJoiner(A,null)&gt;</td>
     * </tr>
     * <tr>
     * <td>&lt;K2:B&gt;</td>
     * <td>&lt;K2:b&gt;</td>
     * <td>&lt;K2:ValueJoiner(B,b)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K3:c&gt;</td>
     * <td></td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} (for one input stream) before doing the
     * join, using a pre-created topic with the "correct" number of partitions.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner).
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen for one or both of the joining {@code KStream}s.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     * <p>
     * Both of the joining {@code KStream}s will be materialized in local state stores with auto-generated store names.
     * For failure and recovery each store will be backed by an internal changelog topic that will be created in Kafka.
     * The changelog topic will be named "${applicationId}-storeName-changelog", where "applicationId" is user-specified
     * in {@link org.apache.kafka.streams.StreamsConfig} via parameter {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG},
     * "storeName" is an internally generated name, and "-changelog" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     *
     * @param otherStream           the {@code KStream} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param windows               the specification of the {@link JoinWindows}
     * @param <VO>                  the value type of the other stream
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key plus one for each non-matching record of
     * this {@code PojoKStream} and within the joining window intervals
     * @see #join(KStream, ValueJoiner, JoinWindows)
     * @see #outerJoin(KStream, ValueJoiner, JoinWindows)
     **/
    <VO, VR> PojoKStream<K, VR> leftJoin(KStream<K, VO> otherStream,
                                         Object pojo,
                                         String valueJoinerMethodName,
                                         Class<VR> toValueClass,
                                         JoinWindows windows);

    /**
     * Join records of this stream with another {@code KStream}'s records using windowed left equi join with default
     * serializers and deserializers.
     * In contrast to {@link #join(KStream, ValueJoiner, JoinWindows) inner-join}, all records from this stream will
     * produce at least one output record (cf. below).
     * The join is computed on the records' key with join attribute {@code thisKStream.key == otherKStream.key}.
     * Furthermore, two records are only joined if their timestamps are close to each other as defined by the given
     * {@link JoinWindows}, i.e., the window defines an additional join predicate on the record timestamps.
     * <p>
     * For each pair of records meeting both join predicates the provided method with name <i>valueJoinerMethodName</i> will be called to compute
     * a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * Furthermore, for each input record of this {@code KStream} that does not satisfy the join predicate the provided
     * {@link ValueJoiner} will be called with a {@code null} value for the other stream.
     * If an input record key or value is {@code null} the record will not be included in the join operation and thus no
     * output record will be added to the resulting {@code PojoKStream}.
     * <p>
     * Example (assuming all input records belong to the correct windows):
     * <table border='1'>
     * <tr>
     * <th>this</th>
     * <th>other</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td>&lt;K1:ValueJoiner(A,null)&gt;</td>
     * </tr>
     * <tr>
     * <td>&lt;K2:B&gt;</td>
     * <td>&lt;K2:b&gt;</td>
     * <td>&lt;K2:ValueJoiner(B,b)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K3:c&gt;</td>
     * <td></td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} (for one input stream) before doing the
     * join, using a pre-created topic with the "correct" number of partitions.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner).
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen for one or both of the joining {@code KStream}s.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     * <p>
     * Both of the joining {@code KStream}s will be materialized in local state stores with auto-generated store names.
     * For failure and recovery each store will be backed by an internal changelog topic that will be created in Kafka.
     * The changelog topic will be named "${applicationId}-storeName-changelog", where "applicationId" is user-specified
     * in {@link org.apache.kafka.streams.StreamsConfig} via parameter {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG},
     * "storeName" is an internally generated name, and "-changelog" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     *
     * @param otherStream           the {@code KStream} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param windows               the specification of the {@link JoinWindows}
     * @param joined                a {@link Joined} instance that defines the serdes to
     *                              be used to serialize/deserialize inputs and outputs of the joined streams
     * @param <VO>                  the value type of the other stream
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key plus one for each non-matching record of
     * this {@code PojoKStream} and within the joining window intervals
     * @see #join(KStream, ValueJoiner, JoinWindows, Joined)
     * @see #outerJoin(KStream, ValueJoiner, JoinWindows, Joined)
     **/
    <VO, VR> PojoKStream<K, VR> leftJoin(KStream<K, VO> otherStream,
                                         Object pojo,
                                         String valueJoinerMethodName,
                                         Class<VR> toValueClass,
                                         JoinWindows windows,
                                         Joined<K, V, VO> joined);


    /**
     * Join records of this stream with another {@code KStream}'s records using windowed outer equi join with default
     * serializers and deserializers.
     * In contrast to {@link #join(KStream, ValueJoiner, JoinWindows) inner-join} or
     * {@link #leftJoin(KStream, ValueJoiner, JoinWindows) left-join}, all records from both streams will produce at
     * least one output record (cf. below).
     * The join is computed on the records' key with join attribute {@code thisKStream.key == otherKStream.key}.
     * Furthermore, two records are only joined if their timestamps are close to each other as defined by the given
     * {@link JoinWindows}, i.e., the window defines an additional join predicate on the record timestamps.
     * <p>
     * For each pair of records meeting both join predicates the provided method with name <i>valueJoinerMethodName</i> will be called to compute
     * a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * Furthermore, for each input record of both {@code KStream}s that does not satisfy the join predicate the provided
     * {@link ValueJoiner} will be called with a {@code null} value for the this/other stream, respectively.
     * If an input record key or value is {@code null} the record will not be included in the join operation and thus no
     * output record will be added to the resulting {@code KStream}.
     * <p>
     * Example (assuming all input records belong to the correct windows):
     * <table border='1'>
     * <tr>
     * <th>this</th>
     * <th>other</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td>&lt;K1:ValueJoiner(A,null)&gt;</td>
     * </tr>
     * <tr>
     * <td>&lt;K2:B&gt;</td>
     * <td>&lt;K2:b&gt;</td>
     * <td>&lt;K2:ValueJoiner(null,b)&gt;<br>&lt;K2:ValueJoiner(B,b)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K3:c&gt;</td>
     * <td>&lt;K3:ValueJoiner(null,c)&gt;</td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} (for one input stream) before doing the
     * join, using a pre-created topic with the "correct" number of partitions.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner).
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen for one or both of the joining {@code KStream}s.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     * <p>
     * Both of the joining {@code KStream}s will be materialized in local state stores with auto-generated store names.
     * For failure and recovery each store will be backed by an internal changelog topic that will be created in Kafka.
     * The changelog topic will be named "${applicationId}-storeName-changelog", where "applicationId" is user-specified
     * in {@link org.apache.kafka.streams.StreamsConfig} via parameter {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG},
     * "storeName" is an internally generated name, and "-changelog" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     *
     * @param otherStream           the {@code KStream} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param windows               the specification of the {@link JoinWindows}
     * @param <VO>                  the value type of the other stream
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key plus one for each non-matching record of
     * both {@code KStream} and within the joining window intervals
     * @see #join(KStream, ValueJoiner, JoinWindows)
     * @see #leftJoin(KStream, ValueJoiner, JoinWindows)
     **/
    <VO, VR> PojoKStream<K, VR> outerJoin(KStream<K, VO> otherStream,
                                          Object pojo,
                                          String valueJoinerMethodName,
                                          Class<VR> toValueClass,
                                          JoinWindows windows);

    /**
     * Join records of this stream with another {@code KStream}'s records using windowed outer equi join with default
     * serializers and deserializers.
     * In contrast to {@link #join(KStream, ValueJoiner, JoinWindows) inner-join} or
     * {@link #leftJoin(KStream, ValueJoiner, JoinWindows) left-join}, all records from both streams will produce at
     * least one output record (cf. below).
     * The join is computed on the records' key with join attribute {@code thisKStream.key == otherKStream.key}.
     * Furthermore, two records are only joined if their timestamps are close to each other as defined by the given
     * {@link JoinWindows}, i.e., the window defines an additional join predicate on the record timestamps.
     * <p>
     * For each pair of records meeting both join predicates the provided method with name <i>valueJoinerMethodName</i> will be called to compute
     * a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * Furthermore, for each input record of both {@code KStream}s that does not satisfy the join predicate the provided
     * {@link ValueJoiner} will be called with a {@code null} value for the this/other stream, respectively.
     * If an input record key or value is {@code null} the record will not be included in the join operation and thus no
     * output record will be added to the resulting {@code KStream}.
     * <p>
     * Example (assuming all input records belong to the correct windows):
     * <table border='1'>
     * <tr>
     * <th>this</th>
     * <th>other</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td>&lt;K1:ValueJoiner(A,null)&gt;</td>
     * </tr>
     * <tr>
     * <td>&lt;K2:B&gt;</td>
     * <td>&lt;K2:b&gt;</td>
     * <td>&lt;K2:ValueJoiner(null,b)&gt;<br>&lt;K2:ValueJoiner(B,b)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K3:c&gt;</td>
     * <td>&lt;K3:ValueJoiner(null,c)&gt;</td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} (for one input stream) before doing the
     * join, using a pre-created topic with the "correct" number of partitions.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner).
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen for one or both of the joining {@code KStream}s.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     * <p>
     * Both of the joining {@code KStream}s will be materialized in local state stores with auto-generated store names.
     * For failure and recovery each store will be backed by an internal changelog topic that will be created in Kafka.
     * The changelog topic will be named "${applicationId}-storeName-changelog", where "applicationId" is user-specified
     * in {@link org.apache.kafka.streams.StreamsConfig} via parameter {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG},
     * "storeName" is an internally generated name, and "-changelog" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     *
     * @param otherStream           the {@code KStream} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param windows               the specification of the {@link JoinWindows}
     * @param joined                a {@link Joined} instance that defines the serdes to
     *                              be used to serialize/deserialize inputs of the joined streams
     * @param <VO>                  the value type of the other stream
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key plus one for each non-matching record of
     * both {@code KStream} and within the joining window intervals
     * @see #join(KStream, ValueJoiner, JoinWindows, Joined)
     * @see #leftJoin(KStream, ValueJoiner, JoinWindows, Joined)
     **/
    <VO, VR> PojoKStream<K, VR> outerJoin(KStream<K, VO> otherStream,
                                          Object pojo,
                                          String valueJoinerMethodName,
                                          Class<VR> toValueClass,
                                          JoinWindows windows,
                                          Joined<K, V, VO> joined);



    /**
     * Join records of this stream with {@link KTable}'s records using non-windowed inner equi join with default
     * serializers and deserializers.
     * The join is a primary key table lookup join with join attribute {@code stream.key == table.key}.
     * "Table lookup join" means, that results are only computed if {@code KStream} records are processed.
     * This is done by performing a lookup for matching records in the <em>current</em> (i.e., processing time) internal
     * {@link KTable} state.
     * In contrast, processing {@link KTable} input records will only update the internal {@link KTable} state and
     * will not produce any result records.
     * <p>
     * For each {@code KStream} record that finds a corresponding record in {@link KTable} the provided
     * method with name <i>valueJoinerMethodName</i> will be called to compute a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * If an {@code KStream} input record key or value is {@code null} the record will not be included in the join
     * operation and thus no output record will be added to the resulting {@code KStream}.
     * <p>
     * Example:
     * <table border='1'>
     * <tr>
     * <th>KStream</th>
     * <th>KTable</th>
     * <th>state</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td></td>
     * <td></td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:b&gt;</td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>&lt;K1:C&gt;</td>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:ValueJoiner(C,b)&gt;</td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} for this {@code KStream} before doing
     * the join, using a pre-created topic with the same number of partitions as the given {@link KTable}.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner);
     * cf. {@link #join(GlobalKTable, KeyValueMapper, ValueJoiner)}.
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen only for this {@code KStream} but not for the provided {@link KTable}.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     *
     * @param table                 the {@link KTable} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param <VT>                  the value type of the table
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key
     * @see #leftJoin(KTable, ValueJoiner)
     * @see #join(GlobalKTable, KeyValueMapper, ValueJoiner)
     **/
    <VT, VR> PojoKStream<K, VR> join(final KTable<K, VT> table,
                                     final Object pojo,
                                     final String valueJoinerMethodName,
                                     final Class<VR> toValueClass);

    /**
     * Join records of this stream with {@link KTable}'s records using non-windowed inner equi join with default
     * serializers and deserializers.
     * The join is a primary key table lookup join with join attribute {@code stream.key == table.key}.
     * "Table lookup join" means, that results are only computed if {@code KStream} records are processed.
     * This is done by performing a lookup for matching records in the <em>current</em> (i.e., processing time) internal
     * {@link KTable} state.
     * In contrast, processing {@link KTable} input records will only update the internal {@link KTable} state and
     * will not produce any result records.
     * <p>
     * For each {@code KStream} record that finds a corresponding record in {@link KTable} the provided
     * method with name <i>valueJoinerMethodName</i> will be called to compute a value (with arbitrary type) for the result record.
     * The key of the result record is the same as for both joining input records.
     * If an {@code KStream} input record key or value is {@code null} the record will not be included in the join
     * operation and thus no output record will be added to the resulting {@code KStream}.
     * <p>
     * Example:
     * <table border='1'>
     * <tr>
     * <th>KStream</th>
     * <th>KTable</th>
     * <th>state</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td></td>
     * <td></td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:b&gt;</td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>&lt;K1:C&gt;</td>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:ValueJoiner(C,b)&gt;</td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} for this {@code KStream} before doing
     * the join, using a pre-created topic with the same number of partitions as the given {@link KTable}.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner);
     * cf. {@link #join(GlobalKTable, KeyValueMapper, ValueJoiner)}.
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen only for this {@code KStream} but not for the provided {@link KTable}.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     *
     * @param table                 the {@link KTable} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param joined                a {@link Joined} instance that defines the serdes to
     *                              be used to serialize/deserialize inputs of the joined streams
     * @param <VT>                  the value type of the table
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one for each matched record-pair with the same key
     * @see #leftJoin(KTable, ValueJoiner, Joined)
     * @see #join(GlobalKTable, KeyValueMapper, ValueJoiner)
     **/
    <VT, VR> PojoKStream<K, VR> join(KTable<K, VT> table,
                                     Object pojo,
                                     String valueJoinerMethodName,
                                     Class<VR> toValueClass,
                                     Joined<K, V, VT> joined);



    /**
     * Join records of this stream with {@link KTable}'s records using non-windowed left equi join with default
     * serializers and deserializers.
     * In contrast to {@link #join(KTable, ValueJoiner) inner-join}, all records from this stream will produce an
     * output record (cf. below).
     * The join is a primary key table lookup join with join attribute {@code stream.key == table.key}.
     * "Table lookup join" means, that results are only computed if {@code KStream} records are processed.
     * This is done by performing a lookup for matching records in the <em>current</em> (i.e., processing time) internal
     * {@link KTable} state.
     * In contrast, processing {@link KTable} input records will only update the internal {@link KTable} state and
     * will not produce any result records.
     * <p>
     * For each {@code KStream} record whether or not it finds a corresponding record in {@link KTable} the provided
     * method with name <i>valueJoinerMethodName</i> will be called to compute a value (with arbitrary type) for the result record.
     * If no {@link KTable} record was found during lookup, a {@code null} value will be provided to {@link ValueJoiner}.
     * The key of the result record is the same as for both joining input records.
     * If an {@code KStream} input record key or value is {@code null} the record will not be included in the join
     * operation and thus no output record will be added to the resulting {@code KStream}.
     * <p>
     * Example:
     * <table border='1'>
     * <tr>
     * <th>KStream</th>
     * <th>KTable</th>
     * <th>state</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td></td>
     * <td>&lt;K1:ValueJoiner(A,null)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:b&gt;</td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>&lt;K1:C&gt;</td>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:ValueJoiner(C,b)&gt;</td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} for this {@code KStream} before doing
     * the join, using a pre-created topic with the same number of partitions as the given {@link KTable}.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner);
     * cf. {@link #join(GlobalKTable, KeyValueMapper, ValueJoiner)}.
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen only for this {@code KStream} but not for the provided {@link KTable}.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     *
     * @param table                 the {@link KTable} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param <VT>                  the value type of the table
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one output for each input {@code PojoKStream} record
     * @see #join(KTable, ValueJoiner)
     * @see #leftJoin(GlobalKTable, KeyValueMapper, ValueJoiner)
     **/
    <VT, VR> PojoKStream<K, VR> leftJoin(KTable<K, VT> table,
                                         Object pojo,
                                         String valueJoinerMethodName,
                                         Class<VR> toValueClass);

    /**
     * Join records of this stream with {@link KTable}'s records using non-windowed left equi join with default
     * serializers and deserializers.
     * In contrast to {@link #join(KTable, ValueJoiner) inner-join}, all records from this stream will produce an
     * output record (cf. below).
     * The join is a primary key table lookup join with join attribute {@code stream.key == table.key}.
     * "Table lookup join" means, that results are only computed if {@code KStream} records are processed.
     * This is done by performing a lookup for matching records in the <em>current</em> (i.e., processing time) internal
     * {@link KTable} state.
     * In contrast, processing {@link KTable} input records will only update the internal {@link KTable} state and
     * will not produce any result records.
     * <p>
     * For each {@code KStream} record whether or not it finds a corresponding record in {@link KTable} the provided
     * method with name <i>valueJoinerMethodName</i> will be called to compute a value (with arbitrary type) for the result record.
     * If no {@link KTable} record was found during lookup, a {@code null} value will be provided to {@link ValueJoiner}.
     * The key of the result record is the same as for both joining input records.
     * If an {@code KStream} input record key or value is {@code null} the record will not be included in the join
     * operation and thus no output record will be added to the resulting {@code KStream}.
     * <p>
     * Example:
     * <table border='1'>
     * <tr>
     * <th>KStream</th>
     * <th>KTable</th>
     * <th>state</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td>&lt;K1:A&gt;</td>
     * <td></td>
     * <td></td>
     * <td>&lt;K1:ValueJoiner(A,null)&gt;</td>
     * </tr>
     * <tr>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:b&gt;</td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>&lt;K1:C&gt;</td>
     * <td></td>
     * <td>&lt;K1:b&gt;</td>
     * <td>&lt;K1:ValueJoiner(C,b)&gt;</td>
     * </tr>
     * </table>
     * Both input streams (or to be more precise, their underlying source topics) need to have the same number of
     * partitions.
     * If this is not the case, you would need to call {@link #through(String)} for this {@code KStream} before doing
     * the join, using a pre-created topic with the same number of partitions as the given {@link KTable}.
     * Furthermore, both input streams need to be co-partitioned on the join key (i.e., use the same partitioner);
     * cf. {@link #join(GlobalKTable, KeyValueMapper, ValueJoiner)}.
     * If this requirement is not met, Kafka Streams will automatically repartition the data, i.e., it will create an
     * internal repartitioning topic in Kafka and write and re-read the data via this topic before the actual join.
     * The repartitioning topic will be named "${applicationId}-XXX-repartition", where "applicationId" is
     * user-specified in {@link org.apache.kafka.streams.StreamsConfig} via parameter
     * {@link org.apache.kafka.streams.StreamsConfig#APPLICATION_ID_CONFIG APPLICATION_ID_CONFIG}, "XXX" is an internally generated name, and
     * "-repartition" is a fixed suffix.
     * You can retrieve all generated internal topic names via {@link org.apache.kafka.streams.KafkaStreams#toString()}.
     * <p>
     * Repartitioning can happen only for this {@code KStream} but not for the provided {@link KTable}.
     * For this case, all data of the stream will be redistributed through the repartitioning topic by writing all
     * records to it, and rereading all records from it, such that the join input {@code KStream} is partitioned
     * correctly on its key.
     *
     * @param table                 the {@link KTable} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param joined                a {@link Joined} instance that defines the serdes to
     *                              be used to serialize/deserialize inputs of the joined streams
     * @param <VT>                  the value type of the table
     * @param <VR>                  the value type of the result stream
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one output for each input {@code PojoKStream} record
     * @see #join(KTable, ValueJoiner, Joined)
     * @see #leftJoin(GlobalKTable, KeyValueMapper, ValueJoiner)
     **/
    <VT, VR> PojoKStream<K, VR> leftJoin(KTable<K, VT> table,
                                         Object pojo,
                                         String valueJoinerMethodName,
                                         Class<VR> toValueClass,
                                         Joined<K, V, VT> joined);



    /**
     * Join records of this stream with {@link GlobalKTable}'s records using non-windowed inner equi join.
     * The join is a primary key table lookup join with join attribute
     * {@code keyValueMapper.map(stream.keyValue) == table.key}.
     * "Table lookup join" means, that results are only computed if {@code KStream} records are processed.
     * This is done by performing a lookup for matching records in the <em>current</em> internal {@link GlobalKTable}
     * state.
     * In contrast, processing {@link GlobalKTable} input records will only update the internal {@link GlobalKTable}
     * state and will not produce any result records.
     * <p>
     * For each {@code KStream} record that finds a corresponding record in {@link GlobalKTable} the provided
     * method with name <i>valueJoinerMethodName</i> will be called to compute a value (with arbitrary type) for the result record.
     * The key of the result record is the same as the key of this {@code KStream}.
     * If a {@code KStream} input record key or value is {@code null} the record will not be included in the join
     * operation and thus no output record will be added to the resulting {@code KStream}.
     * If {@code keyValueMapper} returns {@code null} implying no match exists, no output record will be added to the
     * resulting {@code PojoKStream}.
     *
     * @param globalKTable          the {@link GlobalKTable} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param selectKeyMethodName   Name of @SelectKey annotated method used to map from the (key, value) of this stream
     *                              to the key of the {@link GlobalKTable}
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param <GK>                  the key type of {@link GlobalKTable}
     * @param <GV>                  the value type of the {@link GlobalKTable}
     * @param <RV>                  the value type of the resulting {@code PojoKStream}
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one output for each input {@code PojoKStream} record
     * @see #leftJoin(GlobalKTable, KeyValueMapper, ValueJoiner)
     **/
    <GK, GV, RV> PojoKStream<K, RV> join(final GlobalKTable<GK, GV> globalKTable,
                                         final Object pojo,
                                         final String selectKeyMethodName,
                                         final String valueJoinerMethodName,
                                         final Class<RV> toValueClass);

    /**
     * Join records of this stream with {@link GlobalKTable}'s records using non-windowed left equi join.
     * In contrast to {@link #join(GlobalKTable, KeyValueMapper, ValueJoiner) inner-join}, all records from this stream
     * will produce an output record (cf. below).
     * The join is a primary key table lookup join with join attribute
     * {@code keyValueMapper.map(stream.keyValue) == table.key}.
     * "Table lookup join" means, that results are only computed if {@code KStream} records are processed.
     * This is done by performing a lookup for matching records in the <em>current</em> internal {@link GlobalKTable}
     * state.
     * In contrast, processing {@link GlobalKTable} input records will only update the internal {@link GlobalKTable}
     * state and will not produce any result records.
     * <p>
     * For each {@code KStream} record whether or not it finds a corresponding record in {@link GlobalKTable} the
     * provided method with name <i>valueJoinerMethodName</i> will be called to compute a value (with arbitrary type) for the result record.
     * The key of the result record is the same as this {@code KStream}.
     * If a {@code KStream} input record key or value is {@code null} the record will not be included in the join
     * operation and thus no output record will be added to the resulting {@code KStream}.
     * If {@code keyValueMapper} returns {@code null} implying no match exists, a {@code null} value will be
     * provided to method with name <i>valueJoinerMethodName</i>.
     * If no {@link GlobalKTable} record was found during lookup, a {@code null} value will be provided to
     * {@link ValueJoiner}.
     *
     * @param globalKTable          the {@link GlobalKTable} to be joined with this stream
     * @param pojo                  The java object that implements the @ValueJoiner annotated methods.
     * @param selectKeyMethodName   Name of @SelectKey annotated method used to map from the (key, value) of this stream
     *                              to the key of the {@link GlobalKTable}
     * @param valueJoinerMethodName Name of @ValueJoiner annotated method that computes the join result for a pair of matching records
     * @param toValueClass          The class of the value that is used in the returned stream.
     * @param <GK>                  the key type of {@link GlobalKTable}
     * @param <GV>                  the value type of the {@link GlobalKTable}
     * @param <RV>                  the value type of the resulting {@code PojoKStream}
     * @return a {@code PojoKStream} that contains join-records for each key and values computed by the given
     * valueJoinerMethod, one output for each input {@code PojoKStream} record
     * @see #join(GlobalKTable, KeyValueMapper, ValueJoiner)
     **/
    <GK, GV, RV> PojoKStream<K, RV> leftJoin(GlobalKTable<GK, GV> globalKTable,
                                             Object pojo,
                                             String selectKeyMethodName,
                                             String valueJoinerMethodName,
                                             Class<RV> toValueClass);

    @Override
    PojoKStream<K, V> filter(Predicate<? super K, ? super V> predicate);

    @Override
    PojoKStream<K, V> filterNot(Predicate<? super K, ? super V> predicate);

    @Override
    <K1> PojoKStream<K1, V> selectKey(KeyValueMapper<? super K, ? super V, ? extends K1> mapper);

    @Override
    <K1, V1> PojoKStream<K1, V1> map(KeyValueMapper<? super K, ? super V, ? extends KeyValue<? extends K1, ? extends V1>> mapper);

    @Override
    <V1> PojoKStream<K, V1> mapValues(ValueMapper<? super V, ? extends V1> mapper);

    @Override
    <K1, V1> PojoKStream<K1, V1> flatMap(KeyValueMapper<? super K, ? super V, ? extends Iterable<? extends KeyValue<? extends K1, ? extends V1>>> mapper);

    @Override
    <V1> PojoKStream<K, V1> flatMapValues(ValueMapper<? super V, ? extends Iterable<? extends V1>> mapper);

    @Override
    PojoKStream<K, V>[] branch(Predicate<? super K, ? super V>... predicates);

    @Override
    <K1> PojoKGroupedStream<K1, V> groupBy(final KeyValueMapper<? super K, ? super V, K1> selector);

    @Override
    <KR> PojoKGroupedStream<KR, V> groupBy(final KeyValueMapper<? super K, ? super V, KR> selector,
                                           final Serialized<KR, V> serialized);

    @Override
    PojoKGroupedStream<K, V> groupByKey();

    @Override
    PojoKGroupedStream<K, V> groupByKey(final Serialized<K, V> serialized);

    @Override
    PojoKStream<K, V> merge(KStream<K, V> stream);


    @Override
    PojoKStream<K, V> through(String topic, Produced<K, V> produced);

    @Override
    PojoKStream<K, V> peek(ForeachAction<? super K, ? super V> action);


    @Override
    PojoKStream<K, V> through(String topic);


    @Override
    <V1, R> PojoKStream<K, R> join(KStream<K, V1> other,
                                   ValueJoiner<? super V, ? super V1, ? extends R> joiner,
                                   JoinWindows windows);

    @Override
    <VO, VR> PojoKStream<K, VR> join(KStream<K, VO> otherStream,
                                     ValueJoiner<? super V, ? super VO, ? extends VR> joiner,
                                     JoinWindows windows,
                                     Joined<K, V, VO> joined);


    @Override
    <V1, R> PojoKStream<K, R> outerJoin(KStream<K, V1> other,
                                        ValueJoiner<? super V, ? super V1, ? extends R> joiner,
                                        JoinWindows windows);

    @Override
    <VO, VR> PojoKStream<K, VR> outerJoin(KStream<K, VO> other,
                                          ValueJoiner<? super V, ? super VO, ? extends VR> joiner,
                                          JoinWindows windows,
                                          Joined<K, V, VO> joined);

    @Override
    <V1, R> PojoKStream<K, R> leftJoin(KStream<K, V1> other,
                                       ValueJoiner<? super V, ? super V1, ? extends R> joiner,
                                       JoinWindows windows);

    @Override
    <VO, VR> PojoKStream<K, VR> leftJoin(KStream<K, VO> other,
                                         ValueJoiner<? super V, ? super VO, ? extends VR> joiner,
                                         JoinWindows windows,
                                         Joined<K, V, VO> joined);

    @Override
    <V1, R> PojoKStream<K, R> join(KTable<K, V1> other,
                                   ValueJoiner<? super V, ? super V1, ? extends R> joiner);

    @Override
    <VT, VR> PojoKStream<K, VR> join(KTable<K, VT> other,
                                     ValueJoiner<? super V, ? super VT, ? extends VR> joiner,
                                     Joined<K, V, VT> joined);

    @Override
    <K1, V1, R> PojoKStream<K, R> leftJoin(GlobalKTable<K1, V1> globalTable,
                                           KeyValueMapper<? super K, ? super V, ? extends K1> keyMapper,
                                           ValueJoiner<? super V, ? super V1, ? extends R> joiner);

    @Override
    <K1, V1, V2> PojoKStream<K, V2> join(GlobalKTable<K1, V1> globalTable,
                                         KeyValueMapper<? super K, ? super V, ? extends K1> keyMapper,
                                         ValueJoiner<? super V, ? super V1, ? extends V2> joiner);

    @Override
    <V1, R> PojoKStream<K, R> leftJoin(KTable<K, V1> other, ValueJoiner<? super V, ? super V1, ? extends R> joiner);

    @Override
    <VT, VR> PojoKStream<K, VR> leftJoin(KTable<K, VT> other,
                                         ValueJoiner<? super V, ? super VT, ? extends VR> joiner,
                                         Joined<K, V, VT> joined);


}
