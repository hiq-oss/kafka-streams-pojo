package se.hiq.oss.kafka.streams.method.parameter;


public class DefaultParameterResolver implements ParameterResolver {

    @Override
    public Object[] resolveParameters(Object... parameters) {
        return parameters;
    }
}
